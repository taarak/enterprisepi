# EnterprisePI
EnterprisePI (pronounced like API, but "Enterprise" in place of the "A") is an framework (and an API for itself) for various enterprise applications, for helping managers and system developers to creating his own tools, helping his business or for selling (part of it profit should be redirected to PrinJ Systems, see the license) his products.

# Usage
This library can be used to creating from simple stock control softwares to (now only dreaming) advanced and high performance controlling software.

# Libraries Used
EnterprisePI uses two libraries: jasypt 1.9.2 and JavaMail 1.4.5. This two libraries are mainly used in UserController class, used to handle email sending and user management.

# Using Plugins
This feature is currently in development, but in final release, you will need the plugin (an xml file) file and an instance of PluginManager class. After that, just parse the plugin file with PluginManager and run the code contained in plugin file. All these are currently in development and probably have bugs, but in an further release (as soon as possible) will the the most flexible and dynamic possible, with as many functions (features) as possible.

# Compiling from source
Since I note errors in the source code after publishing a version sometimes (is a bad habit, publishing all instantly after finishing, not doing a full test), generally the most recent version will not be in Downloads section, and you will need to compile all by yourself. To do this, a section in Wiki will be just for that.

# License
PrinJ Systems licensing will be officialized, but any software created with this library need to have the permission of PrinJ Systems to be selled.
