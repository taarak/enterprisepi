/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.selling;

import com.prinjsystems.enterprisepi.User;
import com.prinjsystems.enterprisepi.mail.Mailer;
import javax.mail.MessagingException;

/**
 * This class defines a supplier for a {@link Product}, and has values that can
 * be used for more information in your application.
 */
public class Supplier implements Cloneable {
	private String name;
	private String ein;
	private String address;
	private String email;
	private String phone;
	
	/**
	 * Default constructor. Defines a value for each variable.
	 * @param name Name of the supplier (company, people).
	 * @param ein Employer Identification Number (EIN) of the supplier.
	 * @param address Address of the supplier.
	 * @param email Commercial email of the supplier.
	 * @param phone Commercial phone of the supplier.
	 */
	public Supplier(String name, String ein, String address, String email, String phone) {
		this.name = name;
		this.ein = ein;
		this.address = address;
		this.email = email;
		this.phone = phone;
	}

	/**
	 * Returns the name of this supplier.
	 * @return Supplier name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the Employer Identification Number (EIN) of this supplier.
	 * @return Supplier EIN.
	 */
	public String getEIN() {
		return ein;
	}
	
	/**
	 * Returns the address of this supplier.
	 * @return Supplier address.
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * Returns the email of this supplier.
	 * @return Supplier email. This email will be used on contactSupplier method.
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Returns the phone of this supplier.
	 * @return Supplier phone.
	 */
	public String getPhone() {
		return phone;
	}
	
	/**
	 * Sends a email by Gmail servers (if the sender email not is from gmail, it will not work)
	 * to email of this supplier.
	 * @param sender Sender user.
	 * @param senderPassword Password of the email of the sender user (cannot be encrypted!).
	 * @param subject Subject of the message.
	 * @param message Message to send to supplier email.
	 * @throws javax.mail.MessagingException If some error occur while creating or sending message
	 * to supplier (if this occur, as first try to fix the problem, check the email of this supplier).
	 */
	public void contactSupplier(User sender, String senderPassword, String subject, String message)
			throws MessagingException {
		Mailer.sendGmail(sender, senderPassword, subject, message, email);
	}
	
	@Override
	public Supplier clone() throws CloneNotSupportedException {
		return (Supplier) super.clone();
	}
}
