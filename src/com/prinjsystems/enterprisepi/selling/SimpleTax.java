/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.selling;

/**
 * Represents a simple tax, only with a value.
 */
public final class SimpleTax extends Tax {
	/**
	 * Default constructor. Need a float value to reference the tax value.
	 * @param value Value of this tax.
	 */
	public SimpleTax(float value) {
		super(value);
	}
}
