/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.selling;

import com.prinjsystems.enterprisepi.utils.EPIException;
import com.prinjsystems.enterprisepi.utils.Sale;
import com.prinjsystems.enterprisepi.utils.SaleController;
import java.io.Serializable;
import java.util.List;

/**
 * Class that handles informations about products.
 */
public class Product implements Serializable {
	private int id;
	private String title;
	private String description;
	private String type;
	private String group;
	private String barcode;
	private int stock;
	private List<Integer> sells;
	private List<Tax> taxes;
	private Supplier supplier;
	private float sellPrice;
	private float buyPrice;
	private float profit;

	/**
	 * Default constructor (the best choice for common products).
	 * @param id ID of this new Product. Try to make it unique, so when working
	 * with {@link Sale} objects, you will not enter in trouble.
	 * @param title Title of the product.
	 * @param description Description for the product.
	 * @param type Type of the product.
	 * @param group Group (higher grade than type) of the product.
	 * @param barcode Barcode number of this Product (as a String because
	 * primitives generally can't hold the number).
	 * @param stock Current (starting) stock of the product.
	 * @param sells Current sells of the product.
	 * @param taxes List of included taxes in this product.
	 * @param supplier Supplier of this product.
	 * @param sellPrice Current (starting) price for selling (used to calculate
	 * profit).
	 * @param buyPrice Current (starting) price for buying (used to calculate
	 * profit).
	 */
	public Product(int id, String title, String description, String type, String group,
			String barcode, int stock, List<Integer> sells, List<Tax> taxes, Supplier supplier,
			float sellPrice, float buyPrice) {
		initialize(id, title, description, type, group, barcode, stock, sells, taxes,
				supplier, sellPrice, buyPrice);
	}

	/**
	 * Updates (create a new instance internally) this product with all data
	 * needed at default constructor, and recalculate the profits over it.
	 * @param id ID of this new Product. Try to make it unique, so when working
	 * with {@link Sale} objects, you will not enter in trouble.
	 * @param title Title of the product.
	 * @param description Description for the product.
	 * @param type Type of the product.
	 * @param group Group (higher grade than type) of the product.
	 * @param barcode Barcode number of this Product (as a String because
	 * primitives generally can't hold the number).
	 * @param stock Current (starting) stock of the product.
	 * @param sells Current sells of the product.
	 * @param taxes List of included taxes in this product.
	 * @param supplier Supplier of this product.
	 * @param sellPrice Current (starting) price for selling (used to calculate
	 * profit).
	 * @param buyPrice Current (starting) price for buying (used to calculate
	 * profit).
	 */
	public synchronized void update(int id, String title, String description,
			String type, String group, String barcode, int stock, List<Integer> sells,
			List<Tax> taxes, Supplier supplier, float sellPrice, float buyPrice) {
		initialize(id, title, description, type, group, barcode, stock, sells, taxes,
				supplier, sellPrice,buyPrice);
	}

	/**
	 * Initializes all paramenters for multiple instance using.
	 */
	private void initialize(int id, String title, String description, String type,
			String group, String barcode, int stock, List<Integer> sells, List<Tax> taxes,
			Supplier supplier, float sellPrice, float buyPrice) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.type = type;
		this.group = group;
		this.barcode = barcode;
		this.stock = stock;
		this.sells = sells;
		this.taxes = taxes;
		this.supplier = supplier;
		this.sellPrice = sellPrice;
		this.buyPrice = buyPrice;

		calculateProfit();
	}

	/**
	 * Used for calculating profit.
	 */
	private void calculateProfit() {
		float bprice = getBuyPriceWithTaxes();
		profit = 100 * (sellPrice - bprice) / bprice;
	}

	/**
	 * Changes the price for selling the product and updates the profit value.
	 * @param sellPrice New price for selling.
	 */
	public synchronized void changeSellPrice(float sellPrice) {
		this.sellPrice = sellPrice;
		calculateProfit();
	}

	/**
	 * Changes the price for buying the product and updates the profit value.
	 * @param buyPrice New price for buying.
	 */
	public synchronized void changeBuyPrice(float buyPrice) {
		this.buyPrice = buyPrice;
		calculateProfit();
	}

	/**
	 * Changes the description of this Product.
	 * @param newDescription New description for this Product.
	 */
	public synchronized void changeDescription(String newDescription) {
		this.description = newDescription;
	}

	/**
	 * Updates the profit value and returns it.
	 * @return The updated profit value.
	 */
	public float getProfit() {
		calculateProfit();
		return profit;
	}

	/**
	 * Gets the title of this Product instance.
	 * @return Title of this product.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the description of this specific product.
	 * @return Description of this product.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the type of this product.
	 * @return Type of this product.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Returns the group of this product.
	 * @return Group of this product.
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Gets and return the barcode of this Product.
	 * @return Barcode of this product.
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * Returns the stock value of this product.
	 * @return Stock value of this product.
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * Returns the sell number of this product.
	 * @return Sells of this product.
	 */
	public List<Integer> getSells() {
		return sells;
	}
	
	/**
	 * Returns a list of taxes of this product.
	 * @return Tax list.
	 */
	public List<Tax> getTaxes() {
		return taxes;
	}
	
	/**
	 * Return sum of all taxes in this product.
	 * @return Sum of all taxes.
	 */
	public float getTaxValues() {
		float value = 0.00f;
		
		for(Tax t : taxes) {
			value += t.getTaxValue();
		}
		
		return value;
	}
	
	/**
	 * Returns a copy of the supplier instance in this object.
	 * @return Clone of product supplier.
	 */
	public Supplier getSupplier() {
		Supplier s = null;
		try {
			s = supplier.clone();
		} catch(CloneNotSupportedException cnse) {
			throw new EPIException(cnse);
		}
		return s;
	}

	/**
	 * Returns the buy price of this product, not including taxes.
	 * @return Buy price of this product.
	 */
	public float getBuyPrice() {
		return buyPrice;
	}
	
	/**
	 * Returns the buy price of this product with all taxes included. If there
	 * is no taxes, then the default buy price is returned.
	 * @return Buy price with taxes of this product.
	 */
	public float getBuyPriceWithTaxes() {
		if(taxes == null || taxes.isEmpty()) {
			return buyPrice;
		} else {
			float totalPrice = buyPrice;
			
			for(Tax t : taxes) {
				totalPrice += t.getTaxValue();
			}
			
			return totalPrice;
		}
	}

	/**
	 * Returns the sell price of this product.
	 * @return Sell price of this product.
	 */
	public float getSellPrice() {
		return sellPrice;
	}
	
	/**
	 * Return the ID of this Product.
	 * @return Product ID.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Removes "quantity" from stock and add "quantity" to sells (it "sell" a
	 * number of products).
	 * @param saleId ID of the sale 
	 * @param s SaleController to get quantity to update.
	 * @throws EPIException Throwed if "quantity" is higher than
	 * stock.
	 */
	public void sell(int saleId, SaleController s) throws EPIException {
		int quantity = s.getSale(saleId).getIDs().get(id);
		if(stock < quantity) {
			throw new EPIException("Inappropriate quantity!");
		}

		stock -= quantity;
		sells.add(saleId);
	}

	/**
	 * Adds "quantity" to stock.
	 * @param quantity Quantity to be received by stock.
	 */
	public void receive(int quantity) {
		stock += quantity;
	}
}
