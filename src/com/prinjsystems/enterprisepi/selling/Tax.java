/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.selling;

/**
 * Controls taxes in products. In it's simplest form, it have a value, that
 * is represented by a <code>float</code>, and operates over it.
 * <p>
 * This class is abstract mainly because it is not supposed to be used in it's
 * original form, but if you want this, you can instance a {@link SimpleTax} object.
 */
public abstract class Tax {
	/**
	 * Value of this tax.
	 */
	protected float value;
	
	/**
	 * Creates a simple Tax, that has only a value.
	 * @param value Monetary value of the tax.
	 */
	public Tax(float value) {
		this.value = value;
	}
	
	/**
	 * Returns the monetary value of the tax.
	 * @return Value of this tax.
	 */
	public final float getTaxValue() {
		return value;
	}
	
	/**
	 * Defines a new value for the value of this tax.
	 * @param value New value of this tax.
	 */
	public void setTax(float value) { // Can be overwritten for other calculations (if needed).
		this.value = value;
	}
}
