/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.selling;

import com.prinjsystems.enterprisepi.Company;

/**
 * Tax with a defined source. The tax is like itself is like {@link SimpleTax},
 * but it has a source, that is a String value that defines the location where
 * the tax is obtained (from government or a provider, for example).
 */
public final class SourceTax extends Tax {
	private Object source;
	
	/**
	 * Creates a default tax but with a source.
	 * @param value Monetary value of this tax.
	 * @param source Location from where this tax comes.
	 */
	public SourceTax(float value, String source) {
		super(value);
		this.source = source;
	}
	
	/**
	 * Creates a tax with a source as a {@link Company}.
	 * @param value Monetary value of this tax.
	 * @param source Company from where this tax comes.
	 */
	public SourceTax(float value, Company source) {
		super(value);
		this.source = source;
	}
	
	/**
	 * Returns the source of this tax. The object returned can only
	 * be an String or a {@link Company}.
	 * @return Tax source.
	 */
	public Object getTaxSource() {
		return source;
	}
}
