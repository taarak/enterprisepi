/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.stockage;

import com.prinjsystems.enterprisepi.selling.Product;
import com.prinjsystems.enterprisepi.selling.Supplier;
import com.prinjsystems.enterprisepi.selling.Tax;
import com.prinjsystems.enterprisepi.utils.Logger;
import com.prinjsystems.enterprisepi.utils.Sale;
import com.prinjsystems.enterprisepi.utils.SaleController;
import java.util.List;

/**
 * This class is created for maximum performance while handle all operations
 * that Product does, and autolog operations.
 */
public final class Stock {
	private volatile Product productToStock;
	private final Logger log;
	private final int id;

	/**
	 * Default constructor for Stock instance. Defines it's ID as the same of the
	 * stored product.
	 * @param productToStock Product that this Stock handles.
	 * @param log Logger instance for automatic logging.
	 */
	public Stock(Product productToStock, Logger log) {
		this.productToStock = productToStock;
		this.log = log;
		this.id = productToStock.getId();
	}

	/**
	 * Returns the ID of this Stock instance.
	 * @return The ID variable of this Stock.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Applies an operation created in one {@link Sale} object into the product
	 * on stock.
	 * @param saleId ID of the Sale.
	 * @param s {@link SaleController} to get the Sale.
	 */
	public void sell(int saleId, SaleController s) {
		int quantity = s.getSale(saleId).getIDs().get(productToStock.getId());
		productToStock.sell(saleId, s);

		if(log != null) {
			log.log("Selled " + quantity + " products of title " + productToStock.getTitle() + " of type "
					+ productToStock.getType() + " of group " + productToStock.getGroup()
					+ " from Stock of ID " + id);
		}
	}

	/**
	 * Add "quantity" to stock of the product and log the operation. If
	 * massReceive is true, creates a new thread with maximum priority for
	 * operation completion.
	 * @param quantity Quantity received of product.
	 */
	public void receive(int quantity) {
		productToStock.receive(quantity);
		
		if(log != null) {
			log.log("Received " + quantity + " products of title " + productToStock.getTitle() + " of type "
					+ productToStock.getType() + " of group " + productToStock.getGroup()
					+ " to Stock of ID " + id);
		}
	}
	
	/**
	 * Returns an copy of the {@link Product} stored here.
	 * @return Copy of base product.
	 */
	public Product getProduct() {
		int id = productToStock.getId();
		String title = productToStock.getTitle();
		String description = productToStock.getDescription();
		String type = productToStock.getType();
		String group = productToStock.getGroup();
		String barcode = productToStock.getBarcode();
		int stock = productToStock.getStock();
		List<Integer> sells = productToStock.getSells();
		List<Tax> taxes = productToStock.getTaxes();
		Supplier supplier = productToStock.getSupplier();
		float sellPrice = productToStock.getSellPrice();
		float buyPrice = productToStock.getBuyPrice();
		Product p = new Product(id, title, description, type, group, barcode, stock, sells, taxes, supplier, sellPrice, buyPrice);
		return p;
	}
}
