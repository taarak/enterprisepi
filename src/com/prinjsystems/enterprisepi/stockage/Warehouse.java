/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.stockage;

import com.prinjsystems.enterprisepi.selling.Product;
import com.prinjsystems.enterprisepi.selling.Supplier;
import com.prinjsystems.enterprisepi.selling.Tax;
import com.prinjsystems.enterprisepi.utils.EPIException;
import com.prinjsystems.enterprisepi.utils.Logger;
import com.prinjsystems.enterprisepi.utils.SaleController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Encapsulation for Stock class, handles as many operations as needed at the
 * same time, with maximum performance (no methods as "synchronized" since this
 * class is made for maximum performance).
 */
public final class Warehouse {
	private volatile Map<Integer, Stock> stockage;
	private SaleController sc;
	private Logger log;
	private final int id;

	/**
	 * Initializes a new Warehouse instance, and put all Stock instances a list
	 * (their place in the list is the same as his ID).
	 * @param stockage Simple array containing all the Stock instances that make
	 * part of this Warehouse.
	 * @param id ID of this Warehouse. Not changes anything internally, but can
	 * be used as control when many Warehouse is being used at the same time.
	 * @param log Logger instance for logging Warehouse instantiation (just for
	 * that).
	 * @param sc SaleController to use in sale() methods of products.
	 */
	public Warehouse(Stock[] stockage, int id, Logger log, SaleController sc) {
		this.stockage = new HashMap<>();
		this.sc = sc;
		for(Stock s : stockage) {
			this.stockage.put(s.getId(), s);
		}

		this.id = id;
		this.log = log;
		log("Warehouse of ID " + id + " created!");
	}
	
	private void log(String s) {
		if(log != null) {
			log.log(s);
		}
	}

	/**
	 * Alternative constructor for Warehouse. Uses a List instead of simple
	 * array for putting Stock instances in place.
	 * @param stockage A List with all Stock instances.
	 * @param id ID of this Warehouse.
	 * @param log Logger instance for logging Warehouse instantiation (just for
	 * that too).
	 * @param sc SaleController to use in sale() methods of products.
	 */
	public Warehouse(List<Stock> stockage, int id, Logger log, SaleController sc) {
		this.stockage = new HashMap<>();
		this.sc = sc;
		stockage.forEach((s) -> this.stockage.put(s.getId(), s));

		this.id = id;
		this.log = log;
		log("Warehouse of ID " + id + " created!");
	}

	/**
	 * Sells a quantity of products in Stock of "stockId" with "massSell"
	 * indicator set false.
	 * @param stockId ID of the Stock instance for selling the products.
	 * @param saleId ID of the Sale object to apply changes.
	 */
	public void sell(int stockId, int saleId) {
		stockage.get(stockId).sell(saleId, sc);
	}

	/**
	 * Receive a quantity of products in Stock of "stockId" with "massReceive"
	 * indicator set false.
	 * @param stockId Id of the Stock instance for receiving the products.
	 * @param quantity Quantity of products to be received.
	 */
	public void receive(int stockId, int quantity) {
		stockage.get(stockId).receive(quantity);
	}
	
	/**
	 * Adds a new {@link Stock} to this Warehouse list.
	 * @param stock Object to add into list.
	 */
	public void addProduct(Stock stock) {
		if(stockage.containsKey(stock.getProduct().getId()) && checkBarcode(stock.getProduct().getBarcode())) {
			throw new EPIException("Another product have the same ID and barcode!");
		} if(stockage.containsKey(stock.getProduct().getId())) {
			throw new EPIException("Another product have the same ID!");
		} if(checkBarcode(stock.getProduct().getBarcode())) {
			throw new EPIException("Another product have the same barcode!");
		}
		stockage.put(stock.getId(), stock);
	}
	
	/**
	 * Creates a new {@link Stock} with p.
	 * @param p Product to store into new Stock.
	 */
	public void addProduct(Product p) {
		if(stockage.containsKey(p.getId()) && checkBarcode(p.getBarcode())) {
			throw new EPIException("Another product have the same ID and barcode!");
		} if(stockage.containsKey(p.getId())) {
			throw new EPIException("Another product have the same ID!");
		} if(checkBarcode(p.getBarcode())) {
			throw new EPIException("Another product have the same barcode!");
		}
		Stock s = new Stock(p, log);
		stockage.put(s.getId(), s);
	}
	
	/**
	 * Creates a new {@link Product} and stores it into a new {@link Stock}.
	 * @param id ID of the new product.
	 * @param title Title of the new product.
	 * @param description Description of the new product.
	 * @param type Type of the new product.
	 * @param group Group of the new product.
	 * @param barcode Barcode of the new product.
	 * @param stock Amount in stock of the new product.
	 * @param sells Sell IDs of the new product.
	 * @param taxes Taxes of the new product.
	 * @param supplier Supplier of the new product.
	 * @param sellPrice Sell price of the new product.
	 * @param buyPrice Buy price of the new product.
	 */
	public void addProduct(int id, String title, String description, String type, String group, String barcode,
			int stock, List<Integer> sells, List<Tax> taxes, Supplier supplier, float sellPrice, float buyPrice) {
		if(stockage.containsKey(id) && checkBarcode(barcode)) {
			throw new EPIException("Another product have the same ID and barcode!");
		} if(stockage.containsKey(id)) {
			throw new EPIException("Another product have the same ID!");
		} if(checkBarcode(barcode)) {
			throw new EPIException("Another product have the same barcode!");
		}
		Product p = new Product(id, title, description, type, group, barcode, stock,
				sells, taxes, supplier, sellPrice, buyPrice);
		Stock s = new Stock(p, log);
		stockage.put(s.getId(), s);
	}
	
	private boolean checkBarcode(String barcode) {
		boolean result = false;
		for(Stock s : stockage.values()) {
			if(s.getProduct().getBarcode().equals(barcode)) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Returns specified {@link Stock} instance.
	 * @param id ID to specify which Stock instance to return.
	 * @return Stock instance.
	 */
	public Stock getStock(int id) {
		return stockage.get(id);
	}
	
	/**
	 * Returns specified {@link Product} instance.
	 * @param id ID to specify which Product instance to return.
	 * @return Product instance.
	 */
	public Product getProduct(int id) {
		return stockage.get(id).getProduct();
	}
	
	/**
	 * Creates an copy of stock list and returns.
	 * @return Copy of Stock list.
	 */
	public List<Stock> getStockage() {
		List<Stock> s = new ArrayList<>(stockage.values());
		return s;
	}
	
	/**
	 * Sum sells from all {@link Stock} instances and return the result.
	 * @return Result from sum of all sells.
	 */
	public int getTotalSells() {
		int total = 0;
		
		for(Stock s : stockage.values()) {
			for(Integer i : s.getProduct().getSells()) {
				for(Integer n : sc.getSale(i).getIDs().values()) {
					total += n;
				}
			}
		}
		
		return total;
	}
	
	/**
	 * Return the SaleController contained inside this Warehouse.
	 * @return SaleController of this Warehouse.
	 */
	public SaleController getSaleController() {
		return sc;
	}
	
	/**
	 * Return the ID of this Warehouse. Most of the time,
	 * the ID will be useless, but can be helpful if you plan to use multiple
	 * Warehouses to different type of products.
	 * @return ID of this Warehouse
	 */
	public int getId() {
		return id;
	}
}
