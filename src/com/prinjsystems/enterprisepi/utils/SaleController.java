/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.utils;

import com.prinjsystems.enterprisepi.selling.Product;
import com.prinjsystems.enterprisepi.stockage.Warehouse;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * Controls how {@link Sale} objects will interact with {@link Product}
 * and can create new sales.
 */
public final class SaleController implements Serializable {
	private final Map<Integer, Sale> sales; // Key is sale ID and value is Sale object.
	private final ZoneId zone;
	
	/**
	 * Creates an default SaleController.
	 */
	public SaleController() {
		sales = new HashMap<>();
		zone = ZoneId.systemDefault();
	}
	
	/**
	 * Insertes an already created {@link Sale} at list.
	 * @param s Sale object.
	 * @param w Warehouse object to update products sells.
	 * @throws java.lang.EPIException If the sale object have the
	 * same ID of another one in the list.
	 */
	public void insertSale(Sale s, Warehouse w) {
		if(sales.containsKey(s.getID())) {
			throw new EPIException("An sale with that ID have "
					+ "been detected!");
		}
		
		sales.put(s.getID(), s);
		
		s.getIDs().keySet().stream().forEach((id) -> w.sell(id, s.getID()));
	}
	
	/**
	 * Returns an cloned {@link Sale} object.
	 * @param saleId ID of the Sale to clone and then return.
	 * @return Clone of an Sale object, stored in saleId position.
	 */
	public Sale getSale(int saleId) {
		return sales.get(saleId).clone();
	}
	
	/**
	 * Clones the sale map that stores all information about Sales and returns it.
	 * @return Cloned version of map containing all Sales.
	 */
	public Map<Integer, Sale> getSaleMap() {
		HashMap<Integer, Sale> clonedMap = new HashMap<>();
		sales.values().forEach((s) -> clonedMap.put(s.getID(), s));
		return clonedMap;
	}
	
	/**
	 * Return the next open ID for Sale.
	 * <p>
	 * Caution when using this! The purpose of this method is for testing, to
	 * minimize the amount of work to do when in development phase.
	 * @return Next open ID for an Sale object.
	 */
	public int getNextSaleID() {
		int returnValue;
		if(sales.containsKey(0)) {
			returnValue = sales.size();
		} else {
			returnValue = sales.size() + 1;
		}
		return returnValue;
	}
	
	/**
	 * Return sum of all gains in sales of this <i>day</i>.
	 * @return Sum of gains at this day.
	 */
	public float getDayGains() {
		float value = 0.00f;
		LocalDate now = LocalDate.now(zone);
		
		for(Sale s : sales.values()) {
			try {
				LocalDate saleDate = s.getSaleDate();
				
				if(saleDate.getDayOfYear() == now.getDayOfYear()) {
					value += s.getPrice();
				}
			} catch(Exception e) {
				continue;
			}
		}
		
		return value;
	}
	
	/**
	 * Return sum of all gains in sales of this <i>month</i>.
	 * @return Sum of gains in this month.
	 */
	public float getMonthGains() {
		float value = 0.00f;
		LocalDate now = LocalDate.now(zone);
		
		for(Sale s : sales.values()) {
			try {
				LocalDate saleDate = s.getSaleDate();
				
				if((saleDate.getMonthValue() == now.getMonthValue()) && (saleDate.getYear() == now.getYear())) {
					value += s.getPrice();
				}
			} catch(Exception e) {
				continue;
			}
		}
		
		return value;
	}
	
	/**
	 * Return sum of all gains in sales of this <i>year</i>.
	 * @return Sum of gains in this year.
	 */
	public float getYearGains() {
		float value = 0.00f;
		LocalDate now = LocalDate.now(zone);
		
		for(Sale s : sales.values()) {
			try {
				LocalDate saleDate = s.getSaleDate();
				
				if(saleDate.getYear() == now.getYear()) {
					value += s.getPrice();
				}
			} catch(Exception e) {
				continue;
			}
		}
		
		return value;
	}
	
	/**
	 * Return sum of all gains in sales of <i>all time</i>.
	 * @return Sum of gains of all time.
	 */
	public float getAllGains() {
		float value = 0.00f;
		
		for(Sale s : sales.values()) {
			value += s.getPrice();
		}
		
		return value;
	}
}
