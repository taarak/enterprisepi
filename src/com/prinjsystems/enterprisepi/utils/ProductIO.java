/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.utils;

import com.prinjsystems.enterprisepi.selling.Product;
import com.prinjsystems.enterprisepi.stockage.Stock;
import com.prinjsystems.enterprisepi.stockage.Warehouse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads and writes products on files, inside an specific folder.
 */
public class ProductIO {
	private String folder;
	private Logger l;
	private Object separatorKey;
	
	/**
	 * Default constructor.
	 * @param folder String var that defines the folder to store products.
	 * @param l Logger for logging write and read actions.
	 */
	public ProductIO(String folder, Logger l) {
		this.folder = folder;
		this.l = l;
		File f = new File(folder);
		f.mkdirs();
	}
	
	/**
	 * Creates a default ProductIO but with a defined separator key.
	 * @param folder String var that defines the folder to store products.
	 * @param l Logger for logging write and read actions.
	 * @param separatorKey Key used to identify when {@link Product} sending stops
	 * and {@link SaleController} object will be sent.
	 */
	public ProductIO(String folder, Logger l, Object separatorKey) {
		this.folder = folder;
		this.l = l;
		File f = new File(folder);
		f.mkdirs();
		setSeparatorKey(separatorKey);
	}
	
	/**
	 * Defines a new key for separating object types sending.
	 * @param separatorKey Key to identify Product and SaleController sending.
	 */
	public void setSeparatorKey(Object separatorKey) {
		this.separatorKey = separatorKey;
	}
	
	/**
	 * Stores an {@link Product} in an file with extension .prd
	 * @param p Product to save.
	 * @param oos Custom output stream. If null, default output stream will be used,
	 * that stores all into an folder defined in constructor.
	 * @throws java.io.IOException If cannot store product in file.
	 */
	public void writeProduct(Product p, ObjectOutputStream oos) throws IOException {
		try {
			File file = new File(folder + p.getTitle().toLowerCase().replaceAll(" ", "_") + ".prd");
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream internalOos = null;
			if(oos != null) {
				internalOos = oos;
			} else {
				internalOos = new ObjectOutputStream(fos);
			}
			internalOos.writeObject(p);
			log("Stored file ", file.getName(), " successfully!");
			if(oos == null) {
				internalOos.close();
			} else {
				internalOos.flush();
			}
		} catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			log("FileNotFoundException storing file!");
		} catch(IOException e) {
			log("IOException storing file!");
			throw new IOException("Exception writing product!");
		}
	}
	
	/**
	 * Stores an {@link Product} inside an {@link Stock} on a file with extension .prd
	 * @param s Where to get the product to save.
	 * @param oos Custom output stream. If null, default output stream will be used,
	 * that stores all into an folder defined in constructor.
	 * @throws java.io.IOException If cannot store product in file.
	 */
	public void writeProduct(Stock s, ObjectOutputStream oos) throws IOException {
		writeProduct(s.getProduct(), oos);
	}
	
	/**
	 * Stores all products inside an {@link Warehouse} with extension .prd
	 * @param w Warehouse to get all products.
	 * @param oos Custom output stream. If null, default output stream will be used,
	 * that stores all into an folder defined in constructor.
	 * @throws java.io.IOException If cannot store product in file.
	 */
	public void writeProducts(Warehouse w, ObjectOutputStream oos) throws IOException {
		for(Stock s : w.getStockage()) {
			writeProduct(s, oos);
		}
		if(oos != null) {
			oos.writeObject(separatorKey);
		}
		storeSales(w.getSaleController(), oos);
	}
	
	/**
	 * Sends a Warehouse object over a Socket.
	 * <p>
	 * This method not sends an entire Warehouse, but sends all it's {@link Stock}
	 * instances, sends the separator key and then the {@link SaleController} object.
	 * @param w Warehouse to send.
	 * @param sock Socket to get connection, to then send Warehouse.
	 * @throws IOException If something strange occurs when sending Warehouse.
	 */
	public void writeProductsToSocket(Warehouse w, Socket sock) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream());
		for(Stock s : w.getStockage()) {
			writeProduct(s, oos);
		}
		oos.writeObject(separatorKey);
		storeSales(w.getSaleController(), oos);
		writeProducts(w, null); // For some reason folder gets corrupt if this operation not is made after writing to socket.
	}
	
	/**
	 * Stores entire {@link SaleController} object, that can be further used to create an Warehouse object
	 * with stored products and the file generated by the program (not change the default name,
	 * the program only can recognizes the file with the default name).
	 * @param sc SaleController object to store.
	 * @param oos Custom OutputStream to write SaleController. Can be null, then default OutputStream
	 * will be used.
	 * @throws java.io.IOException If SaleController cannot be stored in an file.
	 */
	public void storeSales(SaleController sc, ObjectOutputStream oos) throws IOException {
		FileOutputStream fos = new FileOutputStream(new File(folder + "salecontroller.obj"));
		ObjectOutputStream internalOos = new ObjectOutputStream(fos);
		if(oos != null) {
			internalOos = oos;
		}
		internalOos.writeObject(sc);
	}
	
	/**
	 * Reads all products inside the specified folder and return an Warehouse with it.
	 * @param wId ID of the new Warehouse.
	 * @return Warehouse instance by reading files contained in folder.
	 * @throws java.io.IOException If something goes wrong when reading files.
	 * @throws java.lang.ClassNotFoundException If class to be read not found.
	 */
	public Warehouse readProducts(int wId) throws IOException, ClassNotFoundException {
		List<Stock> stockage = new ArrayList<>();
		SaleController sc;
		
		for(File readFile : new File(folder).listFiles()) {
			if(readFile.getName().equals("salecontroller.obj")) {
				continue;
			}
			
			Product p = (Product) readProduct(readFile);
			Stock s = new Stock(p, l);
			stockage.add(s);
		}
		
		sc = readSales();
		
		log("Successfully readed products!");
		return new Warehouse(stockage, wId, l, sc);
	}
	
	/**
	 * Reads a Warehouse object being sent by a Socket. Uses the same pattern described in
	 * writeProducts(Warehouse w, Socket sock) method.
	 * @param wId ID of the returned Warehouse.
	 * @param sock Socket to get data.
	 * @return Warehouse instance readed from Socket.
	 * @throws IOException If something strange occurs when reading from Socket.
	 * @throws ClassNotFoundException If Warehouse class or any other used not found.
	 */
	public Warehouse readProducts(int wId, Socket sock) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
		List<Stock> stockage = new ArrayList<>();
		Object currP = null;
		while((currP = ois.readObject()) != separatorKey && !currP.equals(separatorKey)) {
			Product p = (Product) currP;
			stockage.add(new Stock(p, l));
		}
		SaleController sc = (SaleController) ois.readObject(); // SaleController expected now, since separator key has been sent.
		return new Warehouse(stockage, wId, l, sc);
	}
	
	/**
	 * Read one specific product and return it.
	 * @param productTitle Title of the product to search (the original title of the product, not
	 * the exact title of the file).
	 * @return The product readed from an file.
	 * @throws IOException If something goes wrong when reading the file.
	 * @throws ClassNotFoundException If the class to cast object (class Product) not found.
	 */
	public Product readProduct(String productTitle) throws IOException, ClassNotFoundException {
		Product rp;
		String title = productTitle.toLowerCase().replaceAll(" ", "_");
		
		if(!title.endsWith(".prd")) {
			title = title + ".prd";
		}
		
		FileInputStream fis = new FileInputStream(new File(folder + title));
		ObjectInputStream ois = new ObjectInputStream(fis);
		rp = (Product) ois.readObject();
		
		return rp;
	}
	
	/**
	 * Read one specific product and return it.
	 * @param f File to read the product.
	 * @return The product readed from an file.
	 * @throws IOException If something goes wrong when reading the file.
	 * @throws java.lang.ClassNotFoundException If the class to cast object (class Product) not found.
	 */
	public Product readProduct(File f) throws IOException, ClassNotFoundException {
		return readProduct(f.getName());
	}
	
	/**
	 * Reads a product being send by an stream. If the data sent from ObjectOutputStream is
	 * not a {@link Product}, or even a object, unexpected results will occur (like error throwing
	 * by not wanted data).
	 * @param ois Input stream to read data.
	 * @return Readed product.
	 * @throws IOException If something goes wrong reading data.
	 * @throws ClassNotFoundException If Product class not found.
	 */
	public Product readProduct(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		return (Product) ois.readObject();
	}
	
	/**
	 * Reads the file containing an {@link SaleController} instance.
	 * @return SaleController readed from file.
	 * @throws IOException If something goes wrong when reading the file.
	 * @throws ClassNotFoundException If the class to cast object (class SaleController) not found.
	 */
	public SaleController readSales() throws IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(new File(folder + "salecontroller.obj"));
		ObjectInputStream ois = new ObjectInputStream(fis);
		return (SaleController) ois.readObject();
	}
	
	private void log(String logLine, String... compl) {
		if(l != null) {
			l.log(logLine, compl);
		}
	}
}
