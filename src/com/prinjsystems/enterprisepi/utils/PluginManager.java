/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Manages plugins based on a source folder (can be any folder, but for more easy
 * read a "plugins" folder in the root directory of the program is recommended).
 */
public class PluginManager {

	private File sourceFolder;
	private File[] plugins;
	private List<Function> functions;
	private Map<String, String> envVars;
	
	private boolean silent;

	/* Problably big class that stores and run plugin code */
	private class Function {

		private String lines;
		private String title;
		private Map<String, String> vars;

		public Function(String lines, String title, Map<String, String> vars)
				throws EPIException {
			this.lines = lines;
			this.title = title;
			this.vars = vars;
			
			if(!(vars instanceof HashMap)) {
				throw new EPIException("Maybe the class cannot work!");
			}
		}
		
		public Function() {
		}

		public void setLines(String lines) {
			this.lines = lines;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}
		
		public void setVars(Map<String, String> vars) throws EPIException {
			this.vars = vars;
			
			if(!(vars instanceof HashMap)) {
				throw new EPIException("Maybe the class cannot work!");
			}
		}
		
		public String getLines() {
			return lines;
		}

		public String getTitle() {
			return title;
		}
		
		public String getVar(String title) {
			return vars.get(title);
		}
		
		public Map<String, String> getVarList() {
			return vars;
		}
		
		public void addVar(String title, String value) {
			vars.put(title, value);
		}

		public boolean run() {
			try {
				String[] code = lines.split(System.lineSeparator());
				
				for(String line : code) {
					line = compileVars(line);
					if(line.startsWith("print ")) {
						/* 6 is the number of character of "print " (with the space) */
						System.out.print(line.substring(6));
					} else if(line.startsWith("println ")) {
						/* 8 is the number of character of "println " (with the space) */
						System.out.println(line.substring(8));
					} else if(line.startsWith("run ")) {
						String className = line.split(" ")[1].split("::")[0];
						String methodName = line.split(" ")[1].split("::")[1];
						String[] args = line.split(" ")[2].split(",");
						
						Class cls = Class.forName(className);
						Object obj = cls.newInstance();
						Class<?>[] argsClass = new Class<?>[args.length];
						
						for(int i = 0; i < args.length; i++) {
							argsClass[i] = args[i].getClass();
						}
						
						Method method = obj.getClass().getMethod(methodName, argsClass);
						method.invoke(obj, (Object[]) args); /* Maybe the conversion not work */
					}
				}
			} catch(Exception e) {
				return false;
			}

			return true;
		}
		
		private String compileVars(String text) {
			for(int i = 0; i < text.length(); i++) {
				if(text.substring(i).startsWith("${")) {
					String varName = text.substring(i + 2, text.indexOf("}"));
					String var = vars.get(varName);
					text = text.replace("${" + varName + "}", var);
				}
			}
			
			return text;
		}
	}

	/**
	 * Default constructor for PluginManager. Defines a source folder of all the
	 * plugin files.
	 * <p>
	 * Recommended use path as <tt>/project_folder/plugins/</tt>, that can more easily be
	 * recognized as a folder for plugins, and of easy access for developers, staff
	 * and even for the program.
	 * @param path Path of the source folder (the folder name must be included,
	 * with an slash at end).
	 */
	public PluginManager(String path) throws EPIException {
		sourceFolder = new File(path);
		plugins = sourceFolder.listFiles();
		functions = new ArrayList<>();
		envVars = new HashMap<>();
		silent = false;
		
		if(!sourceFolder.isDirectory()) {
			throw new EPIException("Path indicated must be a folder!");
		}
	}

	/**
	 * Returns the absolute path of the root folder (source folder, is the same
	 * thing, at least here).
	 * @return Absolute path of the root folder.
	 */
	public String getFolderPath() {
		return sourceFolder.getAbsolutePath();
	}
	
	/**
	 * Define if will operate in silence.
	 * <p>
	 * If this value is true, no parse text will be print, and if <tt>listVars()</tt>
	 * method be explicity called, only var names and values will be print, not denoting
	 * what is name or value (name comes first and value comes after) and not printing
	 * what function own the variable.
	 * @param silent Defines if the manager will operate in silent mode.
	 */
	public void setSilent(boolean silent) {
		this.silent = silent;
	}
	
	/**
	 * Return true if the class is operating in silence.
	 * <p>
	 * If <tt>silent</tt> is true (class operating is silence) the return value
	 * is true, otherwise will be false.
	 * @return Silent mode value.
	 */
	public boolean isSilent() {
		return silent;
	}

	/**
	 * Parses all the functions contained in an plugin defined by pluginName,
	 * and stores it in an list of all parsed functions.
	 * <p>
	 * Parameter <tt>pluginName</tt> can have or not the <tt>.xml</tt> extension, if
	 * not have, the method will automatically put it.
	 * @param pluginName Name of the plugin to parse.
	 */
	public void parse(String pluginName) {
		if(!pluginName.endsWith(".xml")) {
			pluginName += ".xml";
		}
		
		File xml = null;
		String syntax = "";

		for(File f : plugins) {
			if(f.getName().equals(pluginName)) {
				xml = f;
				break;
			}
		}

		try {
			DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xml);

			doc.getDocumentElement().normalize();

			if(!doc.getDocumentElement().getNodeName().equals("plugin")) {
				throw new Exception();
			}

			NodeList list = doc.getElementsByTagName("function");
			NodeList varList = doc.getElementsByTagName("var");
			Map<String, String> vars = new HashMap<>();
			
			/*
			 *
			 * FUNCTION PARSING
			 *
			 */
			/* Outer loop */
			for(int i = 0; i < list.getLength(); i++) {
				Node n = list.item(i);
				NodeList childList = n.getChildNodes();
//				System.out.println(n.getNodeName() + " " + n.getNodeValue()
//						+ " " + n.getNodeType()); /* Debug */

				/* Inner loop */
				for(int ii = 0; ii < childList.getLength(); ii++) {
					
					String lineSyntax = null;
					Node nn = childList.item(ii);
//					System.out.println(nn.getNodeName().replace("#", "")); /* Debug */
					
					NamedNodeMap attr = nn.getAttributes();

					switch(nn.getNodeName().replace("#", "")) {
						case "printl":
							lineSyntax = "println " + attr.getNamedItem("value").getNodeValue();
							break;
						case "print":
							lineSyntax = "print " + attr.getNamedItem("value").getNodeValue();
							break;
						case "exec":
							lineSyntax = "run " + attr.getNamedItem("class").getNodeValue() + "::"
									+ attr.getNamedItem("method").getNodeValue() + " "
									+ attr.getNamedItem("args").getNodeValue();
							break;
						case "var":
							vars.put(attr.getNamedItem("name").getNodeValue(),
									attr.getNamedItem("value").getNodeValue());
							break;
						default:
							lineSyntax = "";
							break;
					}
					
					lineSyntax += System.lineSeparator();
					syntax += lineSyntax;
				}
				
				//System.out.println(syntax); /* Debug */
				String title = n.getAttributes().getNamedItem("name").getNodeValue();
				Function f = new Function();
				f.setLines(syntax);
				f.setTitle(title);
				f.setVars(vars);
				functions.add(f);
				vars = new HashMap<>();
				syntax = "";
				if(!silent) System.out.println("Function " + title + " parsed!");
			}
			
			/*
			 *
			 * ENVIRONMENT VARIABLE PARSING
			 *
			 */
			for(int i = 0; i < varList.getLength(); i++) {
				Node n = varList.item(i);
				NamedNodeMap attr = n.getAttributes();
				
				if(!n.getParentNode().getNodeName().equals("plugin")) {
					continue;
				}
				
				String title = attr.getNamedItem("name").getNodeValue();
				String value = attr.getNamedItem("value").getNodeValue();
				
				envVars.put(title, value);
				
				if(!silent) {
					System.out.println("Environment variable " + title + " parsed!");
				}
			}
			
			for(Function f : functions) {
				for(Map.Entry<String, String> e : envVars.entrySet()) {
					f.addVar(e.getKey(), e.getValue());
				}
			}
		} catch(Exception e) {
			System.err.println("Error parsing " + pluginName + "!");
			System.err.println(e.getMessage());
			System.exit(-1);
		}
	}
	
	/**
	 * Runs an function (only can run if you have an plugin with the specified
	 * function and parsed this plugin) and return it exit status.
	 * @param functionTitle Title of the function to be run.
	 * @return Exit status of the function (if an exception is throw, the execution
	 * stops and an false value is returned).
	 * @throws EPIException Throw an exception if no function with
	 * functionTitle exists.
	 */
	public boolean runFunction(String functionTitle) throws EPIException {
		Function func = null;
		
		for(Function f : functions) {
			if(f.getTitle().equals(functionTitle)) {
				func = f;
				break;
			}
		}
		
		if(func == null) {
			throw new EPIException("Function titled " + functionTitle + " not exists!");
		}
		
		return func.run();
	}
	
	/**
	 * Shows every variable of every function.
	 * <p>
	 * For example:
	 * You have two functions, "func1" and "func2", each of these have
	 * some variables, and the result afters parsing the file containing this
	 * functions and running <tt>listVars()</tt> will be:
	 * <pre>
	 * Function title: func1
	 * Name: exampl1    Value: someValue
	 * Name: exampl2    Value: justString
	 * Name: exampl3    Value: onePlusOne
	 * Function title: func2
	 * Name: otherExpl1    Value: theString
	 * </pre>
	 * Or if the class is operating in silence, the output will be:
	 * <pre>
	 * exampl1 someValue
	 * exampl2 justString
	 * exampl3 onePlusOne
	 * otherExpl1 theString
	 * </pre>
	 */
	public void listVars() {
		for(Function f : functions) {
			if(!silent) System.out.println("Function title: " + f.getTitle());
			for(Map.Entry<String, String> e : f.getVarList().entrySet()) {
				if(!silent) {
					System.out.println("Name: " + e.getKey() + "    Value: " + e.getValue());
				} else {
					System.out.println(e.getKey() + " " + e.getValue());
				}
			}
		}
	}
	
//	/* DEBUG */
//	public void listFunctions() {
//		for(Function f : functions) {
//			System.out.println("Name: " + f.getTitle());
//			System.out.println(f.getLines());
//		}
//	}
}
