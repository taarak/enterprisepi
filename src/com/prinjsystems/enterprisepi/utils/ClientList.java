/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

import com.prinjsystems.enterprisepi.Client;
import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Arrays;

/**
 * This class stores a list of Client.
 * @param <T> Client class (or an extension) for storing.
 */
public final class ClientList<T extends Client> extends AbstractList<T> {
	private final T[] clients;
	
	/**
	 * Constructs new ClientList with predefined array.
	 * @param array Predefined array to use.
	 */
	public ClientList(T[] array) {
		clients = array;
	}
	
	/**
	 * Constructs a new ClientList with an specific maximum size.
	 * @param c Class to create array (just use T.class).
	 * @param maximumSize Maximum size of this List.
	 */
	public ClientList(Class<T> c, int maximumSize) {
		clients = (T[]) Array.newInstance(c, maximumSize);
	}
	
	/**
	 * Constructs a new ClientList with 100000 of maximum size (my computer have 4 gigs
	 * of RAM, more than these will explode my memory for some reason).
	 * @param c Class to create array (just use T.class).
	 */
	public ClientList(Class<T> c) {
		clients = (T[]) Array.newInstance(c, 100000); /* 100,000, my computer does not
				have memory for larger numbers */
	}
	
	/**
	 * Adds a new element to end of list.
	 * @param element Element to be added.
	 * @return False if can't add, or true if added.
	 */
	@Override
	public boolean add(T element) {
		try {
			int index = -1;
			
			for(int i = 0; i < size(); i++) {
				if(clients[i] == null) {
					index = i;
					break;
				}
			}
			
			clients[index] = element;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Gets an value contained in index position.
	 * @param index Index of value.
	 * @return Value of index in list.
	 */
	@Override
	public T get(int index) {
		return clients[index];
	}
	
	/**
	 * Defines value of index to element.
	 * @param index Index of element to replace (even if previous element is null,
	 * will replace it).
	 * @param element Element to put in place of old one.
	 * @return Old element contained in slot (can be null).
	 */
	@Override
	public T set(int index, T element) {
		T old = clients[index];
		clients[index] = element;
		return old;
	}
	
	/**
	 * Returns index of some element (uses equals function to do this).
	 * @param o Object to compare.
	 * @return Index of found element (or -1 if not found).
	 */
	@Override
	public int indexOf(Object o) {
		int index = -1;
		
		for(int i = 0; i < size(); i++) {
			if(clients[i].equals(o)) {
				index = i;
			}
		}
		
		return index;
	}
	
	/**
	 * Size of the list.
	 * @return Size of this list.
	 */
	@Override
	public int size() {
		return clients.length;
	}
	
	/**
	 * Returns array representation of this list.
	 * @return Array of this list.
	 */
	@Override
	public Object[] toArray() {
		return (Object[]) clients.clone();
	}
	
	/**
	 * Hash code of this list.
	 * @return Calculated hash code of this list.
	 */
	@Override
	public int hashCode() { // Just copied from List javadoc.
		int hashCode = 1;
		for (T e : clients) {
			hashCode = 31*hashCode + (e==null ? 0 : e.hashCode());
		}
		
		return hashCode;
	}

	/**
	 * Compares another ClientList to this list.
	 * @param obj Other list.
	 * @return If the comparation is successful.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ClientList<?> other = (ClientList<?>) obj;
		if(!Arrays.deepEquals(this.clients, other.clients)) {
			return false;
		}
		
		return true;
	}
}
