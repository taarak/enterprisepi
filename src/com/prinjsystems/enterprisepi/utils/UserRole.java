/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.utils;

import java.io.Serializable;

/**
 * Defines an role for an {@link com.prinjsystems.enterprisepi.User}.
 */
public final class UserRole implements Serializable {
	private final String role;
	private int adminLevel;
	
	/**
	 * Represents a Seller.
	 */
	public static final int SELLER = 0;

	/**
	 * Represents a Manager.
	 */
	public static final int MANAGER = 1;

	/**
	 * Represents a System Admin/Admin. Minimum power level for doing risk operations in UserController.
	 */
	public static final int ADMIN = 2;

	/**
	 * Represents owner of the commerce. Maximum power level, except from System (from default ones).
	 */
	public static final int OWNER = 3;
	
	/**
	 * Default constructor.
	 * @param role Role to store.
	 * @param adminLevel Level of administration.
	 */
	public UserRole(String role, int adminLevel) {
		this.role = role;
		this.adminLevel = adminLevel;
	}
	
	/**
	 * Return the role stored in this object.
	 * @return Role type of some {@link com.prinjsystems.enterprisepi.User}.
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * Return administration state.
	 * @return Current state of administration level.
	 */
	public int getAdminLevel() {
		return adminLevel;
	}
}
