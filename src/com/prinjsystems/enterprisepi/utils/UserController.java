/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

import com.prinjsystems.enterprisepi.Client;
import com.prinjsystems.enterprisepi.User;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 * Used for controlling user activities.
 */
public final class UserController {
	private User currentUser;
	private User bufferUser;
	private String configsFile;
	private final String regex;
	private final File file;
	private Logger logger;
	private final Map<String, String[]> lines;
	private ClientList<? extends Client> cList;
	private Map<String, User> users;
	private boolean useSerialization;
	private boolean writeInFile;
	//private final Pattern filePattern;

	// 1 are username, 2 are password, 3 are role, 4 are payment, 5 are email, 6 are phone, 7 are zip code and 8 are power level.
	private int username, password, role, payment, email, phone, zip, power;
	
	/**
	 * Username value to store and check in configuration file.
	 */
	public static final int USERNAME = 1;

	/**
	 * Password value to store and check in configuration file.
	 */
	public static final int PASSWORD = 2;

	/**
	 * Role value to store and check in configuration file.
	 */
	public static final int ROLE = 3;

	/**
	 * Payment value to store and check in configuration file.
	 */
	public static final int PAYMENT = 4;
	
	/**
	 * Email value to store and check in configuration file.
	 */
	public static final int EMAIL = 5;
	
	/**
	 * Phone value to store and check in configuration file.
	 */
	public static final int PHONE = 6;
	
	/**
	 * Zip value to store and check in configuration file.
	 */
	public static final int ZIP = 7;
	
	/**
	 * Power level value to store and check in configuration file.
	 */
	public static final int POWER = 8;

	private class UserInfo {

		private String name, password, email, phone, zip;
		private UserRole role;
		private float payment;

		public UserInfo(String name, String password, String email, String phone, String zip, UserRole role, float payment) {
			this.name = name;
			this.password = password;
			this.role = role;
			this.payment = payment;
			this.email = email;
			this.phone = phone;
			this.zip = zip;
		}

		public String getName() {
			return name;
		}

		public String getPassword() {
			return password;
		}

		public UserRole getRole() {
			return role;
		}

		public float getPayment() {
			return payment;
		}

		public String getEmail() {
			return email;
		}

		public String getPhone() {
			return phone;
		}

		public String getZip() {
			return zip;
		}
	}

	/**
	 * Default constructor for UserController.
	 * <p>
	 * This constructor will make serialization use true as default, need to
	 * call setSerialization method to change this.
	 * @param filePath Path for the configuration file (one that only has
	 * accounts).
	 * @param regex Separator string used in configuration file.
	 * @param indices Indices for spliting configuration file. 1 are username, 2
	 * are password, 3 are role and 4 are payment.
	 * @throws java.io.IOException If the file not exists and the program fails to create it
	 * (if this is occurring at any time without solution, check the integrity of the
	 * disk and if the filesystem used by you is supported by Java).
	 */
	public UserController(String filePath, String regex, int[] indices) throws IOException {
		file = new File(filePath);
		if(!file.exists()) {
			file.createNewFile();
		}
		
		users = new HashMap<>();
		currentUser = new User(UserRoles.getRole(0), "System", "system", null, null, null, 0.000f);
		bufferUser = null;
		this.regex = regex;
		cList = new ClientList<>(Client.class);

		lines = new HashMap<>();

		useSerialization = true;

		for(int i = 0; i < indices.length; i++) {
			switch(indices[i]) {
				case 1:
					username = i;
					break;
				case 2:
					password = i;
					break;
				case 3:
					role = i;
					break;
				case 4:
					payment = i;
					break;
				case 5:
					email = i;
					break;
				case 6:
					phone = i;
					break;
				case 7:
					zip = i;
					break;
				case 8:
					power = i;
					break;
				default:
					break;
			}
		}

		loadConfigs(filePath);
	}

	/**
	 * Constructor that add the ability to system log events.
	 * @param filePath Path for the configuration file (one that only has
	 * accounts).
	 * @param regex Separator string used in configuration file.
	 * @param indices Indices for spliting configuration file. 1 are username, 2
	 * are password, 3 are role and 4 are payment.
	 * @param logger Logger instance for system logging.
	 * @deprecated Impossible to complete, since Logger needs a UserController to
	 * be constructed.
	 */
	public UserController(String filePath, String regex,
			int[] indices, Logger logger) {
		file = new File(filePath);
		this.logger = logger;
		currentUser = new User(UserRoles.getRole(0), "System", "system", null, null, null, 0.000f);
		bufferUser = null;
		this.regex = regex;
		cList = new ClientList<>(Client.class);

		lines = new HashMap<>();

		//this.filePattern = Pattern.compile(filePattern);

		for(int i = 0; i < indices.length; i++) {
			switch(indices[i]) {
				case 1:
					username = i;
					break;
				case 2:
					password = i;
					break;
				case 3:
					role = i;
					break;
				case 4:
					payment = i;
					break;
				case 5:
					email = i;
					break;
				case 6:
					phone = i;
					break;
				case 7:
					zip = i;
					break;
				case 8:
					power = i;
					break;
				default:
					break;
			}
		}

		loadConfigs(filePath);
	}

	private void loadConfigs(String filePath) {
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			
			String line;
			StringBuilder buf = new StringBuilder();

			while((line = br.readLine()) != null) {
				if(!line.startsWith("#")) { // Comment line;
					buf.append(line);
				}
			}

			configsFile = buf.toString();
		} catch(Exception e) {
			System.err.println("Error loading file " + filePath + "!");
			System.exit(-1);
		}

		if(!useSerialization) {
			for (String s : configsFile.split("\n")) {
				lines.put(s.split(regex)[username], s.split(regex));
			}
		} else {
			try {
				FileInputStream fis = new FileInputStream(file);
				ObjectInputStream is = new ObjectInputStream(fis);
				
				while(true) {
					User u = (User) is.readObject();
					
					users.put(u.getName(), u);
				}
			} catch(Exception e) {}
		}
	}
	
	/**
	 * Defines a new value for useSerialization.
	 * <p>
	 * If this value is true, all {@link User} data will be serialized and
	 * stored in an file when requested (registering an user), if false will store
	 * all data (including cryptographed password) in an raw file.
	 * @param useSerialization New value for useSerialization
	 */
	public void setSerialization(boolean useSerialization) {
		this.useSerialization = useSerialization;
	}

	/**
	 * Changes the current user, if the password is correct. WARNING! If the
	 * name and password are correct, the bufferUser will be set null!
	 * @param name Name for searching the user in configuration file.
	 * @param password Password for logging into.
	 * @throws EPIException When the user not exists or the password
	 * is wrong.
	 */
	public synchronized void login(String name, String password) throws
			EPIException {
		UserInfo info = null;
		try {
			if(!useSerialization) {
				info = new UserInfo(lines.get(name)[username], lines.get(name)[this.password],
						lines.get(name)[this.email], lines.get(name)[this.phone],
						lines.get(name)[this.zip], new UserRole(lines.get(name)[role],
						Integer.parseInt(lines.get(name)[power])), Float.parseFloat(lines.get(name)[payment]));
			} else {
				User u = users.get(name);
				info = new UserInfo(u.getName(), u.getPassword(), u.getEmail(),
						u.getPhone(), u.getZip(), u.getRole(), u.getPayment());
			}
			
			if(info == null) {
				throw new Exception();
			}
		} catch(Exception e) {
			log("Try to login on user that not exists!!!");

			throw new EPIException("User not exists!");
		}

		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();

		if(encryptor.checkPassword(password, info.getPassword())) {
			bufferUser = null;
			currentUser = new User(info.getRole(), info.getName(),
					info.getPassword(), info.getEmail(), info.getPhone(), info.getZip(), info.getPayment());
		} else {
			log("Wrong password on try to login!!!");

			throw new EPIException("WRONG PASSWORD!!!");
		}
	}

	/**
	 * Logins to user in buffer.
	 */
	public void loginBuffer() {
		this.currentUser = bufferUser;
	}

	/**
	 * Get the pointer for the Logger used in this class.
	 * @return The Logger pointer.
	 */
	public Logger getLogger() {
		return logger;
	}
	
	/**
	 * Defines a new {@link Logger} for UserController, to can log actions.
	 * @param logger Logger instance.
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	/**
	 * Register a new user with simple constructor.
	 * @param role Role of the new user.
	 * @param name Name of the new user.
	 * @param password Non-encrypted password of the new user.
	 * @param email Email of the new user.
	 * @param phone Phone number of the new user.
	 * @param payment Payment of the new user.
	 * @param zip Zip code of the new user.
	 * @throws IncorrectUserException If the current user not is an system
	 * admin.
	 * @throws IllegalStateException If the user exists.
	 * @throws java.io.IOException If some Java internal I/O error occurs.
	 */
	public synchronized void register(UserRole role, String name, String password,
			String email, String phone, String zip, float payment)
			throws IncorrectUserException, IllegalStateException, IOException {
		boolean exists = false;

		try {
			if(!useSerialization) {
				if(lines.get(name) != null) {
					exists = true;
				} else {
					throw new Exception();
				}
			} else {
				if(users.get(name) != null) {
					exists = true;
				} else {
					throw new Exception();
				}
			}
		} catch(Exception e) {
			exists = false;
		}
		
		if(exists) {
			log("Tried to create user that exists!");
			throw new IllegalStateException("This user exists!");
		}
		
		if(currentUser.getRole().getAdminLevel() < 3) {
			log("Severe error trying to create user with incorrect user type!");

			throw new IncorrectUserException("You not have the power level to do this!");
		}

		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();

		User newUser = new User(role, name, encryptor.encryptPassword(password), email, phone, zip, payment);
		bufferUser = newUser;

		String str = "";
		
		if(writeInFile && useSerialization) {
			users.put(bufferUser.getName(), bufferUser);
			writeToFile();
			return;
		}

		for(int i = 0; i < 7; i++) {
			if(username == i) {
				str += bufferUser.getName() + regex;
			} else if(this.password == i) {
				str += bufferUser.getPassword() + regex;
			} else if(this.role == i) {
				str += bufferUser.getRole() + regex;
			} else if(this.payment == i) {
				str += bufferUser.getPayment() + regex;
			} else if(this.email == i) {
				str += bufferUser.getEmail() + regex;
			} else if(this.phone == i) {
				str += bufferUser.getPhone() + regex;
			} else if(this.zip == i) {
				str += bufferUser.getZip() + regex;
			}
		}
		
		str += System.lineSeparator();
		
		if(writeInFile) {
			Files.write(file.toPath(), str.substring(0, str.length() - 1).getBytes(),
					StandardOpenOption.APPEND);
		}
	}
	
	/**
	 * Removes one user of list.
	 * <p>
	 * Caution! This can be harmful, and use only if some user (worker) is officially
	 * not working more (this means that the user not need more to be inside list). Only
	 * System Administrators can do this.
	 * @param name Name of the user to remove.
	 * @throws com.prinjsystems.enterprisepi.utils.IncorrectUserException If current user
	 * not are an System Administrator, throw this exception.
	 * @throws java.io.IOException If got errror writing changes to file.
	 */
	public void removeUser(String name) throws IncorrectUserException, IOException {
		if(currentUser.getRole().getAdminLevel() < 3) {
			log("Failed to remove an User (incorrect role)!");
			throw new IncorrectUserException("Only System Administrators can remove other Users!");
		}
		
		users.remove(name);
		writeToFile();
	}

	/**
	 * Returns the role of the current logged user.
	 * @return Role of the current user.
	 */
	public UserRole getUserRole() {
		return currentUser.getRole();
	}
	
	/**
	 * Returns the current user (the one logged in).
	 * @return Current user.
	 */
	public User getCurrentUser() {
		return currentUser;
	}
	
	/**
	 * Creates a copy of original map and return it.
	 * <p>
	 * This method creates a copy of the original map, to prevent changes in User data.
	 * @return Map containing all users.
	 */
	public Map<String, User> getAllUsers() {
		Map<String, User> newMap = new HashMap<>(users);
		return newMap;
	}
	
	/**
	 * Defines a new {@link ClientList} to using here (to control clients).
	 * @param cList New ClientList instance (of generic type anything that has
	 * as superclass {@link Client}, or even Client itself).
	 */
	public void setClientList(ClientList<? extends Client> cList) {
		this.cList = cList;
	}
	
	private void log(String text) {
		if(logger != null) {
			logger.log(text);
		}
	}
	
	/**
	 * Excludes all {@link User} data, if it is serialized or not.
	 * <p>
	 * <b>!!!CAUTION!!!</b> This operation can be extremely harmful, and not
	 * make one instance of this class accessible to anyone! If you are the system
	 * engineer, even with the security of only System Administrators can access this,
	 * make your instance of this private and without getter, for more
	 * security.
	 * @throws com.prinjsystems.enterprisepi.utils.IncorrectUserException If the current
	 * user not is an System Administrator, will fail and throw this exception.
	 */
	public void deleteAllUsers() throws IncorrectUserException {
		if(currentUser.getRole().getAdminLevel() < 3) {
			log("Extremelly severe error! Someone tried to delete user data file, but failed"
					+ " because he not has this privilege");
			throw new IncorrectUserException("Only system administrators can do this!!!");
		}
		
		users = new HashMap<>();
		
		boolean delete = file.delete();
		
		if(!delete) {
			log("Cannot delete file!");
		} else {
			log("Deleted all users from system!");
		}
	}
	
	/**
	 * Saves all users in configuration file.
	 * <p>
	 * If something goes wrong internally in user handling methods, you can fix
	 * externally and run this method.
	 * Atention! Currently, this method only writes with serialization, if you are
	 * not using serialization for some good reason (if the reason is not too good,
	 * use serialization), this method will throw an exception!
	 * @throws java.io.IOException If got error writing user data to file.
	 * @throws java.lang.IllegalStateException If not using serialization, because this
	 * method <i>only</i> write with serialization.
	 */
	public void writeToFile() throws IOException, IllegalStateException {
		if(!useSerialization) {
			log("Tried to write to file using serialization without permission!");
			throw new IllegalStateException("Needs to use serialization to complete"
					+ " writing to file!");
		}
		
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		
		List<User> usersList = new ArrayList<>(users.values());
		for(User u : usersList) {
			os.writeObject(u);
		}
		
		os.close();
		
		log("Successfully write users data to file!");
	}
	
	/**
	 * Defines if the code will write users in a file with register or not.
	 * @param writeInFile Value that determine the file writing state.
	 */
	public void setWriteInFile(boolean writeInFile) {
		this.writeInFile = writeInFile;
	}
	
	/**
	 * Return the state of file writing operation.
	 * @return If the register method will write to file.
	 */
	public boolean getWriteInFile() {
		return writeInFile;
	}
}
