/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for making user roles definition easier. Do not instantiate the class!
 * All methods are static!
 */
public final class UserRoles {
	private final static List<UserRole> ROLES;
	
	static {
		ROLES = new ArrayList<>();
		ROLES.add(new UserRole("System", Integer.MAX_VALUE));
	}
	
	private UserRoles() {}
	
	/**
	 * Adds an UserRole to the list.
	 * @param role Adds a new role for the list.
	 */
	public static void addRole(UserRole role) {
		ROLES.add(role);
	}
	
	/**
	 * Returns the role contained in some position.
	 * @param index Position to check.
	 * @return UserRole of that position.
	 */
	public static UserRole getRole(int index) {
		return ROLES.get(index);
	}
	
	/**
	 * Returns the first role with the title.
	 * @param title Title of the role.
	 * @return UserRole with the title (the first one).
	 */
	public static UserRole getRole(String title) {
		UserRole r = null;
		for(UserRole ur : ROLES) {
			if(ur.getRole().equals(title)) {
				r = ur;
				break;
			}
		}
		return r;
	}
	
	/**
	 * Return all the list with all roles (an copy of).
	 * @return List of all roles.
	 */
	public static List<UserRole> getRoles() {
		List<UserRole> list = new ArrayList<>(ROLES);
		return list;
	}
}
