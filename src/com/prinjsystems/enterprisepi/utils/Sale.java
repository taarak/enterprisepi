/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

import com.prinjsystems.enterprisepi.selling.Product;
import com.prinjsystems.enterprisepi.selling.Tax;
import com.prinjsystems.enterprisepi.stockage.Stock;
import com.prinjsystems.enterprisepi.stockage.Warehouse;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Stores data about one sale, including price, products IDs and date of the sale (including
 * hours, seconds and milliseconds).
 */
public class Sale implements Serializable {
	private final int id;
	private final float price;
	private final List<Tax> taxes;
	private final Map<Integer, Integer> ids; // Key is product ID and value is it`s quantity.
	private final LocalDate saleDate;
	
	/**
	 * Creates an Sale object.
	 * @param id ID of this Sale, to be identified by the program and user.
	 * @param price Price of the sale (without taxes).
	 * @param ids All IDs of all products included in sale.
	 * @param taxes List of all taxes from all products.
	 * @param saleDate Date of the sale.
	 */
	public Sale(int id, float price, Map<Integer, Integer> ids, List<Tax> taxes,
			LocalDate saleDate) {
		this.id = id;
		this.price = price;
		this.taxes = taxes;
		this.ids = ids;
		this.saleDate = saleDate;
	}
	
	/**
	 * Creates an Sale object with the current date with default ZoneId.
	 * @param id ID of this Sale, to be identified by the program and user.
	 * @param price Price of the sale (without taxes).
	 * @param ids All IDs of all products included in sale.
	 * @param taxes List of all taxes from all products.
	 */
	public Sale(int id, float price, Map<Integer, Integer> ids, List<Tax> taxes) {
		this.id = id;
		this.price = price;
		this.taxes = taxes;
		this.ids = ids;
		this.saleDate = LocalDate.now();
	}
	
	/**
	 * Creates an Sale object with the current date.
	 * @param id ID of this Sale, to be identified by the program and user.
	 * @param price Price of the sale (without taxes).
	 * @param ids All IDs of all products included in sale.
	 * @param taxes List of all taxes from all products.
	 * @param zone ZoneId to use as base when setting sale date.
	 */
	public Sale(int id, float price, Map<Integer, Integer> ids, List<Tax> taxes,
			ZoneId zone) {
		this.id = id;
		this.price = price;
		this.taxes = taxes;
		this.ids = ids;
		this.saleDate = LocalDate.now(zone);
	}
	
	/**
	 * Creates a Sale object automatically inserting all taxes and price.
	 * @param id ID of this sale.
	 * @param ids IDs of all products in this sale.
	 * @param w Warehouse to get all taxes data and price from all products.
	 * @param saleDate Date when this sale occurred.
	 */
	public Sale(int id, Map<Integer, Integer> ids, Warehouse w, LocalDate saleDate) {
		this.id = id;
		float price = 0.00f;
		List<Tax> taxes = new ArrayList<>();
		for(Stock s : w.getStockage()) {
			Product p = s.getProduct();
			if(ids.containsKey(p.getId())) {
				for(Tax t : p.getTaxes()) {
					taxes.add(t);
				}
				price += p.getSellPrice();
			}
		}
		this.price = price;
		this.taxes = taxes;
		this.ids = ids;
		this.saleDate = saleDate;
	}
	
	/**
	 * Return the stored price of this Sale (without taxes).
	 * @return Price of this Sale.
	 */
	public float getPrice() {
		return price;
	}
	
	/**
	 * Return the stored price of this Sale (with taxes).
	 * @return Price of this Sale with taxes.
	 */
	public float getPriceWithTaxes() {
		float value = price;
		for(Tax t : taxes) {
			value += t.getTaxValue();
		}
		return value;
	}
	
	/**
	 * Return entire list of taxes from this sale.
	 * @return Tax list.
	 */
	public List<Tax> getTaxes() {
		return taxes;
	}
	
	/**
	 * Return an HashMap containing all IDs of all products in this Sale.
	 * The keys are {@link Product} IDs and the values are they amount.
	 * @return List all products IDs and their amounts.
	 */
	public HashMap<Integer, Integer> getIDs() {
		HashMap<Integer, Integer> clone = new HashMap<>();
		clone.putAll(ids);
		return clone;
	}
	
	/**
	 * Return the reference ID of this Sale.
	 * @return ID that references this Sale.
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Return the date of the sale with precision of milliseconds.
	 * @return Date of the sale.
	 */
	public LocalDate getSaleDate() {
		return saleDate;
	}
	
	/**
	 * Creates an exact clone of this Sale object.
	 * @return Clone of this object.
	 */
	@Override
	public Sale clone() {
		return new Sale(id, price, ids, taxes, saleDate);
	}
}
