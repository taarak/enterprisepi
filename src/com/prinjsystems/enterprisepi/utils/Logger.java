/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * This class does automatic relatories (without needing user to decide what to
 * log), but can do mannual logging.
 */
public class Logger {
	private StringBuilder currentLog;
	private String header;
	private final String defaultHeader;
	private boolean writeTime;
	private final ZoneId zoneId;
	private final String folderPath;
	private final UserController controller;
	private final int id;

	/**
	 * Default cosntructor to Logger class.
	 * @param id Id of the logger. Not does anything internally, but can be used
	 * to something external.
	 * @param folderPath Path for the log folder (need a folder separator at
	 * final).
	 * @param writeTime If true, the automatic logging service will log the
	 * current time in hour, minute and second after the header and before the
	 * name and actions.
	 * @param zoneId ZoneId for checking the correct clock. Only needed if the
	 * function getTime() will be used or writeTime is true, otherwise can be
	 * null.
	 * @param controller User controller used to get user role in automatic log.
	 */
	public Logger(int id, String folderPath, boolean writeTime, ZoneId zoneId, UserController controller) {
		restartBuffer();
		this.defaultHeader = "[$log][$username] ";
		this.header = null;
		this.id = id;
		this.folderPath = folderPath;
		this.writeTime = writeTime;
		this.zoneId = zoneId;
		this.controller = controller;
	}

	/**
	 * Creates a log line, with default header (the default header is
	 * "[AUTOLOG][$username] $name $compl").
	 * @param name Name of this log line (comes first, after header).
	 * @param compl Complements of this log line (comes after name).
	 */
	public void log(String name, String... compl) {
		String time = "";

		if(writeTime) {
			time = getTime();
		}
		
		String thisHeader = defaultHeader;
		
		if(header != null) {
			thisHeader = header;
		}
		
		thisHeader = thisHeader.replace("$username", controller.getUserRole().getRole().toUpperCase().replaceAll(" ", "_"))
					.replace("$log", "AUTOLOG");

		String log = thisHeader + time + " " + name;

		for(String s : compl) {
			log += " " + s;
		}

		log += System.lineSeparator();

		currentLog.append(log);
	}
	
	/**
	 * Defines a new header for log lines.
	 * <p>
	 * To remove the custom log header (use the default one), set this value to
	 * null, and the default header will be used.
	 * <p>
	 * Note that user name will replace tag {@code $username} and the <i>AUTOLOG</i>
	 * text will replace tag {@code $log}.
	 * @param header New header for log lines (what comes before log text).
	 */
	public void setHeader(String header) {
		if(!header.endsWith(" ")) {
			header = header + " ";
		}
		
		this.header = header;
	}
	
	/**
	 * Returns the current header in use.
	 * <p>
	 * If custom header is null, returns the default header.
	 * @return Header in use.
	 */
	public String getHeader() {
		if(header != null) {
			return header;
		}
		
		return defaultHeader;
	}

	/**
	 * Creates a new, absolutelly custom, log line (without header).
	 * @param log Log line to be added.
	 */
	public void mannualLog(String log) {
		currentLog.append(log);
	}

	/**
	 * Creates a new, absolutelly custom, log line (without header) with an
	 * carriage return at end.
	 * @param log Log line to be added.
	 */
	public void mannualLogLn(String log) {
		String logging = log + System.lineSeparator();

		currentLog.append(logging);
	}

	/**
	 * Returns the ID of this Logger instance.
	 * @return The stored ID of this Logger.
	 */
	public int getId() {
		return id;
	}

	private void restartBuffer() {
		currentLog = new StringBuilder();
		System.gc();
		String log = "[AUTOLOG] LOGGER ID " + id + " STARTED" + System.lineSeparator();
		currentLog.append(log);
	}

	/**
	 * Writes the current stored log into a file (with name
	 * "log-$year-$month-$day.log"), restarts the log buffer and return a
	 * boolean value that tells if the operation is successful.
	 * @return If the operation is successful, return true, otherwise false.
	 */
	public boolean writeLog() {
		boolean success = false;

		LocalDateTime now = LocalDateTime.now();

		int year = now.getYear();
		int month = now.getMonthValue();
		int day = now.getDayOfMonth();

		try(BufferedWriter bw = new BufferedWriter(new FileWriter(
				new File(folderPath + "log-" + year + "-" + month + "-" + day + ".log")))) {
			bw.write(currentLog.toString());

			restartBuffer();

			success = true;
		} catch(Exception e) {
			System.err.println("Error writing log " + year + "-" + month + "-" + day + "!");
			System.exit(-1);
		}

		return success;
	}

	/**
	 * Gets and return the current time of the system in a string (in the form
	 * hour:minute:second).
	 * @return Current time based on system.
	 */
	public String getTime() {
		LocalDateTime now = LocalDateTime.now(zoneId);
		String hour = String.valueOf(now.getHour());
		String minute = String.valueOf(now.getMinute());
		String second = String.valueOf(now.getSecond());
		return hour + ":" + minute + ":" + second;
	}

	/**
	 * Returns the ZoneId set in constructor.
	 * @return ZoneId set in constructor.
	 */
	public ZoneId getZoneId() {
		return zoneId;
	}

	/**
	 * Change the value of writeTime. If true, when automatic logging, the
	 * logger will put the current time inside each log line.
	 * @param writeTime New value for writeTime field.
	 */
	public void setWriteTime(boolean writeTime) {
		this.writeTime = writeTime;
	}

	/**
	 * Returns the value of writeTime.
	 * @return writeTime value.
	 */
	public boolean getWriteTime() {
		return writeTime;
	}
}
