/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.utils;

/**
 * Throwed if you try to make something with wrong user.
 */
public class IncorrectUserException extends Exception {
	/**
	 * Exception without message.
	 */
	public IncorrectUserException() {}
	
	/**
	 * Exception throw with an message.
	 * @param message Message of this exception.
	 */
	public IncorrectUserException(String message) {
		super(message);
	}
}
