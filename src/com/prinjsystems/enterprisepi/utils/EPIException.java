/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.utils;

/**
 * Exception for EnterprisePI related problems (except for those who use {@link IncorrectUserException}.
 */
public class EPIException extends RuntimeException {

	/**
	 * Creates a defuault EPIException.
	 */
	public EPIException() {
		super();
	}
	
	/**
	 * Creates a EPIException with a message.
	 * @param message Message to come with EPIException.
	 */
	public EPIException(String message) {
		super(message);
	}
	
	/**
	 * Creates a EPIException with a cause.
	 * @param cause Cause of EPIException.
	 */
	public EPIException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates a EPIException with a message <i>and</i> a cause.
	 * @param message Message to come with EPIException.
	 * @param cause Cause of this EPIException.
	 */
	public EPIException(String message, Throwable cause) {
		super(message, cause);
	}
}
 