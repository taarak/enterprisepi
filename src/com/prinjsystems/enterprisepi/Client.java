/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi;

/**
 * Class for storing data about clients.
 */
public class Client {
	private final String name;
	private String email;
	private String phoneNumber;
	private String zip;

	/**
	 * Default constructor for Client. Sets an name, email, phone number and zip
	 * code.
	 * @param name Name of this Client.
	 * @param email Email of this Client.
	 * @param phoneNumber Phone of this Client.
	 * @param zip Zip code of this Client.
	 */
	public Client(String name, String email, String phoneNumber, String zip) {
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.zip = zip;
	}

	/**
	 * Changes the email value of this Client.
	 * @param newEmail New email for this Client.
	 */
	public synchronized void updateEmail(String newEmail) {
		email = newEmail;
	}

	/**
	 * Updates the phone number of this Client.
	 * @param newPhone New phone number for this Client.
	 */
	public synchronized void updatePhoneNumber(String newPhone) {
		phoneNumber = newPhone;
	}
	
	/**
	 * Updates the zip code of this Client.
	 * @param newZip New zip code for this Client.
	 */
	public synchronized void updateZipCode(String newZip) {
		zip = newZip;
	}

	/**
	 * Gets and return the name of this Client.
	 * @return Name of this Client.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets and return the email of this Client.
	 * @return Email of this client.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets and return the phone number of this Client.
	 * @return The phone number of this Client.
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Return the zip code of this Client.
	 * @return Zip of this Client.
	 */
	public String getZip() {
		return zip;
	}
}
