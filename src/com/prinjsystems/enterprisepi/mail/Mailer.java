/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.mail;

import com.prinjsystems.enterprisepi.User;
import com.prinjsystems.enterprisepi.utils.EPIException;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * This class can send emails to other people with Gmail services.
 * <p>
 * Password encryption is recommended when sending emails, to be decrypted
 * before calling the method, to prevent email invasion, probrably delaying
 * sales.
 */
public class Mailer {
	private String host;
	private String port;
	
	private static boolean useStarttls;
	
	/**
	 * Constructs a default Mailer. Not is needed unless you will use any other
	 * email server than Gmail.
	 * @param host Host of the email server (like "smtp.gmail.com" for Gmail servers).
	 * @param port Port of the email server (for example 465 for Gmail servers).
	 */
	public Mailer(String host, String port) {
		this.host = host;
		this.port = port;
	}
	
	/**
	 * Define if the sender will use STARTTLS.
	 * @param starttls STARTTLS value.
	 */
	public static void setStarttls(boolean starttls) {
		useStarttls = starttls;
	}
	
	/**
	 * Return value of STARTTLS. If true, email sending will use STARTTLS.
	 * @return STARTTLS value.
	 */
	public static boolean usingStarttls() {
		return useStarttls;
	}
	
	/**
	 * Return email host.
	 * @return Email host.
	 */
	public String getHost() {
		return host;
	}
	
	/**
	 * Return email host port.
	 * @return Port of email host.
	 */
	public String getPort() {
		return port;
	}
	
	/**
	 * Defines email host to send custom emails.
	 * @param host Email host.
	 */
	public void setHost(String host) {
		this.host = host;
	}
	
	/**
	 * Defines email host port to send custom emails.
	 * @param port Port of the email host.
	 */
	public void setPort(String port) {
		this.port = port;
	}
	
	/**
	 * Sends an email with a subject to anyone, with a custom host and port, and
	 * authenticating sender.
	 * <p>
	 * Be aware of any authenticating errors, not listed in throws!
	 * @param sender Sender user.
	 * @param password Password of the sender. <b>Can't</b> be encrypted!
	 * @param subject Subject of the message.
	 * @param message Message to send.
	 * @param dests Array of emails to send message.
	 * @throws MessagingException If any errors occur during creating or sending
	 * message.
	 */
	public void sendEmail(User sender, String password, String subject, String message,
			String... dests) throws MessagingException {
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", String.valueOf(useStarttls));
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", port);

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(sender.getEmail(), password);
			}
		});
		session.setDebug(true);
		Message message0 = new MimeMessage(session);
		message0.setFrom(new InternetAddress(sender.getEmail()));
		
		StringBuilder emails = new StringBuilder();
		String prefix = "";
		for(String s : dests) {
			emails.append(prefix).append(s);
			if(prefix.equals("")) {
				prefix = ", ";
			}
		}
		
		Address[] toUser = InternetAddress
				.parse(emails.toString());
		message0.setRecipients(Message.RecipientType.TO, toUser);
		message0.setSubject(subject);
		message0.setText(message);
		Transport.send(message0);
	}
	
	/**
	 * Sends a message with subject to anyone with a custom host and port, but
	 * without authenticating sender.
	 * <p>
	 * If the email server doesn't care about who is sending the email, you can set
	 * this to anything (but try to use or empty string, "", or a email finishing
	 * with the server domain).
	 * @param sender Sender email (can be nothing, if the server doesn't care).
	 * @param subject Subject of the message.
	 * @param message Message to send.
	 * @param dests Array of emails to send message.
	 * @throws MessagingException If any errors occur during creating or sending
	 * message.
	 */
	public void sendEmail(String sender, String subject, String message,
			String... dests) throws MessagingException {
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", String.valueOf(useStarttls));
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", port);

		Session session = Session.getDefaultInstance(props);
		session.setDebug(true);
		Message message0 = new MimeMessage(session);
		message0.setFrom(new InternetAddress(sender));
		
		StringBuilder emails = new StringBuilder();
		String prefix = "";
		for(String s : dests) {
			emails.append(prefix).append(s);
			if(prefix.equals("")) {
				prefix = ", ";
			}
		}
		
		Address[] toUser = InternetAddress
				.parse(emails.toString());
		message0.setRecipients(Message.RecipientType.TO, toUser);
		message0.setSubject(subject);
		message0.setText(message);
		Transport.send(message0);
	}

	/**
	 * Sends a message with a subject to emails of any provider with a Gmail account.
	 * <p>
	 * Attention when writing email handler (if any)! The password <i>can't</i> be
	 * encrypted! If you have encrypted your password, decrypt it right before calling
	 * this method.
	 * <p>
	 * This method is static due to it simplicity, not requiring any special parameters,
	 * but any other methods that sends a email with a custom email server not are
	 * static, to prevent entering server configuration at every email sending.
	 * <p>
	 * Be aware of any authenticating errors, not listed in throws!
	 * @param sender Sender user.
	 * @param password Password. <b>Can't</b> be encrypted!
	 * @param subject Subject of the message.
	 * @param message Message to send.
	 * @param dests Emails to send the message with subject.
	 * @throws com.prinjsystems.enterprisepi.utils.EPIException If sender user email not is from
	 * Gmail.
	 * @throws javax.mail.MessagingException If any errors occur during creating
	 * or sending message.
	 */
	public static void sendGmail(User sender, String password, String subject, String message,
			String... dests) throws EPIException, MessagingException {
		if(!sender.getEmail().contains("@gmail.com")) {
			throw new EPIException("Sender email need to be from Gmail!");
		}
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", String.valueOf(useStarttls));
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(sender.getEmail(), password);
			}
		});
		session.setDebug(true);
		Message message0 = new MimeMessage(session);
		message0.setFrom(new InternetAddress(sender.getEmail()));
		
		StringBuilder emails = new StringBuilder();
		String prefix = "";
		for(String s : dests) {
			emails.append(prefix).append(s);
			if(prefix.equals("")) {
				prefix = ", ";
			}
		}
		
		Address[] toUser = InternetAddress
				.parse(emails.toString());
		message0.setRecipients(Message.RecipientType.TO, toUser);
		message0.setSubject(subject);
		message0.setText(message);
		Transport.send(message0);
	}
}
