/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi;

import com.prinjsystems.enterprisepi.utils.EPIException;

/**
 * This class stores information about companies. Used as a field in Bill class.
 */
public class Company {
	private String name;
	private String address;
	private String email;
	private int workers;

	/**
	 * Default constructor for Company class. Sets the default fields.
	 * @param name Name of the company.
	 * @param address Address of the company.
	 * @param email Email of the company.
	 * @param workers Number of workers of the company.
	 * @exception EPIException If the number of workers is equal or
	 * less than 0.
	 */
	public Company(String name, String address, String email, int workers)
			throws EPIException {
		this.name = name;
		this.address = address;
		this.email = email;

		if(workers <= 0) {
			throw new EPIException("Number of workers must be greater "
					+ "of zero!");
		} else {
			this.workers = workers;
		}
	}

	/**
	 * Alternative constructor for Company class. Not define the number of
	 * workers.
	 * @param name Name of the company.
	 * @param address Address of the company.
	 * @param email Email of the company.
	 */
	public Company(String name, String address, String email) {
		this.name = name;
		this.address = address;
		this.email = email;
		this.workers = -1;
	}

	/**
	 * Returns the name of this company.
	 * @return Name of this company.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets and return the address of this company.
	 * @return Address of this company.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Returns the email of this company.
	 * @return Email of this company.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets and return the number of workers of this company.
	 * @return Number of workers of this company (if -1, the alternative
	 * constructor has been used).
	 */
	public int getWorkers() {
		return workers;
	}
}
