/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.billing;

import com.prinjsystems.enterprisepi.Client;
import com.prinjsystems.enterprisepi.utils.ClientList;
import java.util.AbstractList;
import java.util.Arrays;

/**
 * Stores bills, like {@link ClientList} do with {@link Client}.
 */
public class Bills extends AbstractList<Bill> {
	private Bill[] bills;
	
	/**
	 * Constructs new ClientList with predefined array.
	 * @param array Predefined array to use.
	 */
	public Bills(Bill[] array) {
		bills = array;
	}
	
	/**
	 * Constructs a new ClientList with an specific maximum size.
	 * @param maximumSize Maximum size of this List.
	 */
	public Bills(int maximumSize) {
		bills = new Bill[maximumSize];
	}
	
	/**
	 * Constructs a new ClientList with 100000 of maximum size (my computer have 4 gigs
	 * of RAM, more than these will explode my memory for some reason).
	 */
	public Bills() {
		bills = new Bill[100000];
	}
	
	/**
	 * Adds a new element to end of list.
	 * @param element Element to be added.
	 * @return False if can't add, or true if added.
	 */
	@Override
	public boolean add(Bill element) {
		try {
			int index = -1;
			
			for(int i = 0; i < size(); i++) {
				if(bills[i] == null) {
					index = i;
					break;
				}
			}
			
			if(index > size()) {
				Bill[] old = new Bill[size()];
				for(int i = 0; i < size(); i++) {
					old[i] = bills[i];
				}
				bills = new Bill[size() + ((int) size() / 4)];
				for(int i = 0; i < old.length; i++) {
					bills[i] = old[i];
				}
			}
			
			bills[index] = element;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Gets an value contained in index position.
	 * @param index Index of value.
	 * @return Value of index in list.
	 */
	@Override
	public Bill get(int index) {
		return bills[index];
	}
	
	/**
	 * Defines value of index to element.
	 * @param index Index of element to replace (even if previous element is null,
	 * will replace it).
	 * @param element Element to put in place of old one.
	 * @return Old element contained in slot (can be null).
	 */
	@Override
	public Bill set(int index, Bill element) {
		Bill old = bills[index];
		bills[index] = element;
		return old;
	}
	
	/**
	 * Returns index of some element (uses equals function to do this).
	 * @param o Object to compare.
	 * @return Index of found element (or -1 if not found).
	 */
	@Override
	public int indexOf(Object o) {
		int index = -1;
		
		for(int i = 0; i < size(); i++) {
			if(bills[i].equals(o)) {
				index = i;
			}
		}
		
		return index;
	}
	
	/**
	 * Size of the list.
	 * @return Size of this list.
	 */
	@Override
	public int size() {
		return bills.length;
	}
	
	/**
	 * Returns array representation of this list.
	 * @return Array of this list.
	 */
	@Override
	public Object[] toArray() {
		return (Object[]) bills.clone();
	}
	
	/**
	 * Hash code of this list.
	 * @return Calculated hash code of this list.
	 */
	@Override
	public int hashCode() { // Just copied from List javadoc.
		int hashCode = 1;
		for (Bill e : bills) {
			hashCode = 31*hashCode + (e==null ? 0 : e.hashCode());
		}
		
		return hashCode;
	}

	/**
	 * Compares another ClientList to this list.
	 * @param obj Other list.
	 * @return If the comparation is successful.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Bills other = (Bills) obj;
		if(!Arrays.deepEquals(this.bills, other.bills)) {
			return false;
		}
		
		return true;
	}
}
