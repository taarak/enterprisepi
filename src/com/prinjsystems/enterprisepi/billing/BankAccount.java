/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.billing;

import com.prinjsystems.enterprisepi.utils.EPIException;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * A class that stores information about how much money you have into your
 * bank account (the business one).
 */
public class BankAccount {
	private float money;
	private NumberFormat converter;
	private static BankAccount singleton; // It's supposed that you will not need two BankAccount instances.
	
	private BankAccount() {
		money = 0.00f;
		converter = NumberFormat.getCurrencyInstance();
		converter.setMinimumFractionDigits(2);
		converter.setMaximumFractionDigits(2);
	}
	
	static {
		singleton = new BankAccount();
	}
	
	/**
	 * Return a instance of BankAccount. 
	 * @return Fresh instance of BankAccount class.
	 */
	public static BankAccount getInstance() {
		return singleton;
	}
	
	/**
	 * Defines the Locale for converting money float value to a string.
	 * @param locale Locale to use as base on convertions.
	 */
	public void setLocale(Locale locale) {
		converter = NumberFormat.getCurrencyInstance(locale);
		converter.setMinimumFractionDigits(2);
		converter.setMaximumFractionDigits(2);
	}
	
	/**
	 * Defines how much money this bank account has.
	 * @param money Money amount.
	 */
	public void setMoney(float money) {
		this.money = money;
	}
	
	/**
	 * Adds parameter float to the internal money value.
	 * @param money Value to add.
	 */
	public void receive(float money) {
		this.money += money;
	}
	
	/**
	 * Returns the amount of money contained in this bank account.
	 * @return Money amount.
	 */
	public float getMoney() {
		return money;
	}
	
	/**
	 * Converts money value to a string representing the value based on Locale passed
	 * in setLocale method.
	 * @return Money value converted to a string.
	 */
	public String getMoneyToString() {
		return converter.format(money);
	}
	
	/**
	 * Pay a bill and removes it's value from stored money amount.
	 * @param b Bill to pay.
	 */
	public void payBill(Bill b) {
		if(b.getValue() > money) {
			throw new EPIException("Bill value is too high! Bank account not have enough money!");
		}
		
		money -= b.getValue();
		b.pay();
	}
	
	/**
	 * Removes value from stored money amount.
	 * @param value Value to remove.
	 */
	public void pay(float value) {
		if(value > money) {
			throw new EPIException("Value is too high! Bank account not have enough money!");
		}
		
		money -= value;
	}
}
