/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi.billing;

import com.prinjsystems.enterprisepi.Company;
import com.prinjsystems.enterprisepi.utils.EPIException;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Fundamental class from billing package. Administrates a single bill, and can
 * be annulled when it is not more useful.
 */
public final class Bill {
	private final int id;
	private float value;
	private Company company;
	private volatile LocalDate expirationDate;
	private LocalDate emissionDate;
	private LocalDate payDate;
	private final ZoneId zoneId;
	private boolean depleted;
	private boolean expired;
	private boolean checkerRunning;

	private final Thread checker;

	/**
	 * Default constructor for Bill class. Defines an ID, an bill value, the
	 * name of the company that is requiring the payment and dates for both,
	 * expiration and emission dates (Note: a thread is created to always check
	 * if the bill is expired).
	 * @param id ID of this bill.
	 * @param value Value of this bill (the amount of money that need be pay).
	 * @param company Company requiring the payment of this bill.
	 * @param expirationDate Date of expiration. Maximum date for paying this
	 * bill.
	 * @param emissionDate Emission date of this bill. When this bill is
	 * created.
	 * @param zoneId Zone id used to compare dates.
	 */
	@SuppressWarnings("CallToThreadStartDuringObjectConstruction")
	public Bill(int id, float value, Company company, LocalDate expirationDate,
			LocalDate emissionDate, ZoneId zoneId) {
		if(emissionDate.isAfter(expirationDate)) {
			throw new EPIException("Emission date is after expiration date!");
		}
		this.id = id;
		this.value = value;
		this.expirationDate = expirationDate;
		this.emissionDate = emissionDate;
		this.payDate = null;
		this.company = company;
		this.zoneId = zoneId;
		expired = false;
		depleted = false;
		checkerRunning = true;

		this.checker = new Thread() {
			@Override
			public void run() {
				while(checkerRunning) {
					LocalDate d = LocalDate.now(zoneId);
					
					if(expirationDate.compareTo(d) >= 0) {
						expired = true;
						return;
					}
					
					if(expired == true) { /* If expire() method is call, the thread dies */
						return;
					}
				}
			}
		};
		checker.setPriority(Thread.MIN_PRIORITY); // This checker doesn't really need to consume too many processing time.
		checker.start();
	}

	/**
	 * Set this bill as depleted, it means that the bill cancels it's relations
	 * with any other classes.
	 */
	public void pay() {
		depleted = true;
		depleteChecker();
		payDate = LocalDate.now(zoneId);
	}
	
	/**
	 * Set this bill as depleted, it means that the bill cancels it's relations
	 * with any other classes, and define a custom pay date.
	 * @param payDate Date when this bill was paid.
	 */
	public void pay(LocalDate payDate) {
		depleted = true;
		depleteChecker();
		this.payDate = payDate;
	}
	
	/**
	 * Stops the checker thread.
	 * <p>
	 * This method is used by {@link BillController} to remove some processing
	 * level required to do this task. Atention! Take care with these, if the program
	 * created with this library uses this bill handler and you deplete one bill checker,
	 * you can forget to pay one bill!
	 * @return If the depletion is successful or failed. If failed, probrably is
	 * because the checker is already depleted.
	 */
	public boolean depleteChecker() {
		try {
			checkerRunning = false;
			checker.join();
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Checks if the checker of this thread is running.
	 * <p>
	 * If it is not running, can be because the bill expired (checker thread automatically
	 * stops if it expires, even with expire() method) or depleteChecker() has been called.
	 * @return If the checker thread is alive.
	 */
	public boolean checkerIsRunning() {
		return checker.isAlive();
	}

	/**
	 * Gets and return the ID of this bill. Not changes, even if the bill is
	 * depleted.
	 * @return ID of this bill.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the value of this bill.
	 * @return Value of this bill.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * Returns the Company that is requiring the payment of this bill.
	 * @return Company that you need to pay.
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Get and return the expiration date of this bill.
	 * @return Expiration date of this bill.
	 */
	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Returns the emission date of this bill (the day that the bill is
	 * created).
	 * @return Emission date of this bill.
	 */
	public LocalDate getEmissionDate() {
		return emissionDate;
	}
	
	/**
	 * Returns the date when this bill was paid.
	 * @return Pay date.
	 */
	public LocalDate getPayDate() {
		return payDate;
	}

	/**
	 * Gets and return the time zone set of this bill.
	 * @return Time Zone of this bill.
	 */
	public ZoneId getZoneId() {
		return zoneId;
	}

	/**
	 * If the bill is depleted, all it's values is null (except ID) and it is
	 * now inactive.
	 * @return If the class is depleted, true, otherwise, false.
	 */
	public boolean isDepleted() {
		return depleted;
	}

	/**
	 * Returns if the bill is expired.
	 * @return If the bill is expired, true, otherwise, false.
	 */
	public boolean isExpired() {
		return expired;
	}
	
	/**
	 * Set isExpired() to true.
	 */
	public void expire() {
		expired = true;
	}
}
