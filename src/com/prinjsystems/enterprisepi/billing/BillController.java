/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */

package com.prinjsystems.enterprisepi.billing;

import java.time.LocalDate;

/**
 * This class controls and reduces processing power needed to run {@link Bill} instances.
 * <p>
 * Every bill instance created here has his checker depleted, and added to an own
 * "public" checker (public meaning to every bill).
 */
public final class BillController {
	private Bills bills;
	
	private Thread publicChecker;
	private boolean checkerRunning;
	
	/**
	 * Creates a new BillController with default size (100,000 bills).
	 */
	public BillController() {
		bills = new Bills();
		initChecker();
	}
	
	/**
	 * Creates a new BillController with custom list size.
	 * @param maximumSize Maximum size of {@link Bill} list.
	 */
	public BillController(int maximumSize) {
		bills = new Bills(maximumSize);
		initChecker();
	}
	
	private void initChecker() {
		checkerRunning = true;
		publicChecker = new Thread(() -> {
			while(checkerRunning) {
				for (Bill b : bills) {
					if(b == null) {
						continue;
					}
					
					LocalDate d = LocalDate.now(b.getZoneId());
					
					if(b.getExpirationDate().compareTo(d) >= 0) {
						b.expire();
					}
				}
			}
		});
		
		publicChecker.start();
	}
	
	/**
	 * Adds a new {@link Bill} to list, and depletes it checker.
	 * @param bill Bill to be added to list.
	 */
	public void addBill(Bill bill) {
		bill.depleteChecker();
		bills.set(bill.getId(), bill);
	}
	
	/**
	 * Returns specified bill by id.
	 * @param billId Id of the bill.
	 * @return Bill instance cointained inside list in position of billId.
	 */
	public Bill getBill(int billId) throws IndexOutOfBoundsException {
		Bill b = bills.get(billId);
		
		if(b == null) {
			throw new IndexOutOfBoundsException("The list not has an bill with this ID!");
		}
		
		return b;
	}
	
	/**
	 * Runs method pay on one bill of specified id.
	 * @param billId Id of the bill.
	 */
	public void payBill(int billId) {
		bills.get(billId).pay();
	}
	
	/**
	 * Returns expiration state of specified id bill.
	 * @param billId Id of the bill.
	 * @return If this bill is expired.
	 */
	public boolean billIsExpired(int billId) {
		return bills.get(billId).isExpired();
	}
	
	/**
	 * Returns depletion state of specified id bill.
	 * @param billId Id of the bill.
	 * @return If this bill is depleted.
	 */
	public boolean billIsDepleted(int billId) {
		return bills.get(billId).isDepleted();
	}
	
	/**
	 * Runs method expire() on one bill of specified id.
	 * @param billId Id of the bill.
	 */
	public void expireBill(int billId) {
		bills.get(billId).expire();
	}
	
	/**
	 * Stops operations of public checker.
	 * @return If the operation is successful.
	 */
	public boolean stopChecker() {
		try {
			checkerRunning = false;
			publicChecker.join();
		} catch (InterruptedException ex) {
			return false;
		}
		
		return true;
	}
}
