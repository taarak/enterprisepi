/*
 * ------------------------------------------------------------
 * -- This code is of property and maintance of PrinJ Systems.
 * ------------------------------------------------------------
 * 
 * Changes are accepted if not changing the original form.
 */
package com.prinjsystems.enterprisepi;

import com.prinjsystems.enterprisepi.utils.UserRole;
import java.io.Serializable;

/**
 * Used for internal workers, like sellers and managers.
 */
public class User implements Serializable {
	private UserRole role;
	private final String name;
	private final String password;
	private final String email;
	private final String phone;
	private final String zip;
	private float payment;

	/**
	 * Used in "payment" variable; when used, this user is considered an slave,
	 * meaning that he is doing community work or voluntaire work ("slave" is
	 * just a way to speak, we use this on HDs too, not go to the wrong side).
	 */
	public static final float SLAVE = -1.111f;

	/**
	 * Simple constructor for workers, not handling age and activating age
	 * deprecation.
	 * @param role Role of this user.
	 * @param name Name of this internal workers.
	 * @param password Encrypted password for the user.
	 * @param email Email of this new user.
	 * @param phone Phone number of this new user.
	 * @param zip Zip code for this user.
	 * @param payment Payment for this user.
	 */
	public User(UserRole role, String name, String password, String email, String phone, String zip, float payment) {
		this.role = role;
		this.name = name;
		this.payment = payment;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.zip = zip;
	}

	/**
	 * Get and return the role of this user.
	 * @return The role of this user.
	 */
	public UserRole getRole() {
		return role;
	}

	/**
	 * Change role of this user (can be promotion or depromotion).
	 * @param promotion New role for this user.
	 */
	public void promote(UserRole promotion) {
		this.role = promotion;
	}

	/**
	 * Get and return the name of this user.
	 * @return The name of this user.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get and return the password of this user (use with care, this can destroy
	 * your application because an fault in your security system).
	 * @return The password of this user.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets and returns the current payment of this user.
	 * @return The current payment of this user.
	 */
	public float getPayment() {
		return payment;
	}

	/**
	 * Changes the payment for this user.
	 * @param newPayment New payment for this user.
	 */
	public synchronized void changePayment(float newPayment) {
		this.payment = newPayment;
	}

	/**
	 * Return the email of this User.
	 * @return User email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Return the phone number of this User.
	 * @return User phone.
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Return the zip code of this User.
	 * @return User zip code.
	 */
	public String getZip() {
		return zip;
	}
}
